import { StatusBar } from 'expo-status-bar';
import React, { useState } from "react";
import { StyleSheet, Text, View, SafeAreaView, Modal, Pressable, Switch, Appearance, Dimensions, Share, ScrollView} from 'react-native';
import SwipeCards from './SwipeCards.js'
//https://reactnavigation.org/docs/material-bottom-tab-navigator/
import { NavigationContainer } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
//https://callstack.github.io/react-native-paper/getting-started.html
import { Provider as PaperProvider, Paragraph, RadioButton } from 'react-native-paper';
// https://nativebase.io/docs/v0.2.0/components#footer
import {Container, Footer, FooterTab, Button, Icon, Title} from 'native-base';












function SettingsScreen() {
  return (
    <View style={styles.container}>
     <Text>This is settings</Text>
    </View>
  )
}

function ShareScreen() {
  return (
    <View style={styles.container}>
     <Text>This is share</Text>
    </View>
  )
}


function AboutScreen() {
  return (
    <View style={styles.container}>
     <Text>This is about</Text>
    </View>
  )
}




const Tab = createMaterialBottomTabNavigator();

// for icons https://reactnavigation.org/docs/bottom-tab-navigator/#example
// https://reactnavigation.org/docs/material-bottom-tab-navigator/#labeled
function MyTabs() {
  return (
    <Tab.Navigator labeled={false} barStyle={{ backgroundColor: 'black' }} >

    <Tab.Screen name="SwipeCards" component={SwipeCards} options={{
        tabBarLabel: 'SwipeCards',
        tabBarIcon: ({ color, size }) => (
          <MaterialIcons name="info" color={color} size={24} style={{ textAlignVertical: 'center' }} />
        ),
      }} />

      <Tab.Screen name="AboutScreen" component={AboutScreen} options={{
          tabBarLabel: 'About',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="info" color={color} size={24} style={{ textAlignVertical: 'center' }} />
          ),
        }} />
      <Tab.Screen name="Settings" component={SettingsScreen} options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="settings" color={color} size={24} />
          ),
        }} />
      <Tab.Screen name="Share" component={ShareScreen} options={{
          tabBarLabel: 'Share',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="share" color={color} size={24} />
          ),
        }} />
    </Tab.Navigator>
  );
}

// maybe we don't even need to use a nav element? Maybe we just want a footer with buttons to go between screens?


// trying to figure out how to just show the card and have the three options instead of the current screen that's showing being one of the three tabs (which is probably how it's meant to work)
//https://reactnavigation.org/docs/hiding-tabbar-in-screens/
function MyHomeTabs() {
  return (
    <Tab.Navigator labeled={false} barStyle={{ backgroundColor: 'black' }} >
      <Tab.Screen name="AboutScreen" component={AboutScreen} options={{
          tabBarLabel: 'About',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="info" color={color} size={24} style={{ textAlignVertical: 'center' }} />
          ),
        }} />
      <Tab.Screen name="Settings" component={SettingsScreen} options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="settings" color={color} size={24} />
          ),
        }} />
      <Tab.Screen name="Share" component={ShareScreen} options={{
          tabBarLabel: 'Share',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="share" color={color} size={24} />
          ),
        }} />
    </Tab.Navigator>
  );
}


const Stack = createStackNavigator();


function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={MyHomeTabs} />
    </Stack.Navigator>
  );
}






export default function App() {


  //https://reactnative.dev/docs/shares
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: currentAphorism
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };


  //https://reactnative.dev/docs/appearance
  // doesn't seem to be working, is always light
  const colorScheme = Appearance.getColorScheme();
  const [infoModalVisible, setInfoModalVisible] = useState(false);
  const [settingsModalVisible, setSettingsModalVisible] = useState(false);
  const [shareModalVisible, setShareModalVisible] = useState(false);
  // for settings, adjusting theme
  // switch button
  const [currentThemeSetInSettings, setThemeSetInSettings] = React.useState(["white", "black"]); // backgroundColor, fontColor 0, 1
  const [isEnabled, setIsEnabled] = useState(false);
  //const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  const toggleSwitch = function () {
    setIsEnabled(previousState => !previousState);
    if (!isEnabled) { // toggling the theme using hook callback
      setThemeSetInSettings(["black", "white"])
    } else {
      setThemeSetInSettings(["white", "black"])
    }
  }


  const [checked, setChecked] = React.useState('first');







  // for handling current aphorism and link button in footer, so the current aphorisms is copied to clipboard for sharing
  const [currentAphorism, setCurrentAphorism] = React.useState("");

  function handleAphorismChange(newValue) {
     setCurrentAphorism(newValue);
   }


   // for settings
   const [currentColorMode, setCurrentColorMode] = React.useState("");


   const [currentFontSize, setCurrentFontSize] = React.useState(18);

   function adjustCurrentFontSize(value) {
     //console.log("value from slider: " + value);
     setCurrentFontSize(value);
   }


/*

https://stackoverflow.com/questions/29914572/react-native-flex-not-responding-to-orientation-change


*/

  return (
    /*
    <SafeAreaView style={styles.container}>
      <PaperProvider>
      */

      <Container>


       <Modal
       supportedOrientations={['portrait', 'landscape']}

        animationType="slide"
        transparent={true}
        visible={infoModalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setInfoModalVisible(!infoModalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          <ScrollView style={styles.scrollView}
        contentContainerStyle={styles.contentContainer} >
          <Text style={{ paddingBottom: 20}}><Title>About</Title></Text>
            <Paragraph>What’s going on? What do you need to hear? How can you move forward?
            {"\n"}
            {"\n"}
            Finding an answer may be as simple as <Text style={{ fontWeight: "bold", textDecorationLine: "underline"}}>swiping left or right.</Text>
            {"\n"}
            {"\n"}
            <Text style={{ fontWeight: "bold", fontStyle: "italic"}}>Forget This Good Thing I Just Said</Text> is a new experience, based on an old kind of book – the collection of short sayings, or aphorisms. By combining several hundred original aphorisms with the ring-oscillator software used in random-number-generating technology, Forget This Good Thing I Just Said offers up a completely unique experience every time you open it.
            {"\n"}
            {"\n"}
            It’s something literary, philosophical, and a little magical to brighten up your screens.
            </Paragraph>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setInfoModalVisible(!infoModalVisible)}
             >
              <MaterialIcons name="close" color={"blue"} size={24} style={{ textAlignVertical: 'center', marginTop: 10 }} />
            </Pressable>
            </ScrollView>
          </View>
        </View>
      </Modal>



      <Modal
      supportedOrientations={['portrait', 'landscape']}
        animationType="slide"
        transparent={true}
        visible={settingsModalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setSettingsModalVisible(!settingsModalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text><Title>Settings</Title></Text>

            <Text style={styles.modalText}>Toggle Dark Mode</Text>

            <View style={{ padding: 20}}>
            <Switch
               trackColor={{ false: "#767577", true: "#81b0ff" }}
               thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
               ios_backgroundColor="#3e3e3e"
               onValueChange={toggleSwitch}
               value={isEnabled}
             />
             </View>

             <Text style={styles.modalText}>Adjust font size</Text>

             <Slider
             style={{width: 200, height: 40}}
             minimumValue={12}
             maximumValue={45}
             minimumTrackTintColor="#81b0ff"
             maximumTrackTintColor="#767577"
             onValueChange={value => adjustCurrentFontSize(value)}
             value={currentFontSize}
             />



            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setSettingsModalVisible(!settingsModalVisible)}
            >
              <MaterialIcons name="close" color={"blue"} size={24} style={{ textAlignVertical: 'center' }} />
            </Pressable>
          </View>
        </View>
      </Modal>



      <Modal
      supportedOrientations={['portrait', 'landscape']}

        animationType="slide"
        transparent={true}
        visible={shareModalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!shareModalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello World!</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setShareModalVisible(!shareModalVisible)}
            >
              <MaterialIcons name="close" color={"blue"} size={24} style={{ textAlignVertical: 'center' }} />
            </Pressable>
          </View>
        </View>
      </Modal>


        <View style={styles.container}>
          <SwipeCards currentStateCallback={setCurrentAphorism} currentTheme={currentThemeSetInSettings} currentFontSize={currentFontSize} />
        </View>

        <Footer style={{ backgroundColor: isEnabled ? "#0a0a0a" : "#ffffff" }}>
            <FooterTab>
                <Button transparent onPress={() => setInfoModalVisible(!infoModalVisible)}>
                  <MaterialIcons name="info" color={'blue'} size={24} />
                </Button>
                <Button transparent onPress={() => setSettingsModalVisible(!settingsModalVisible)} >
                  <MaterialIcons name="settings" color={'blue'} size={24} />
                </Button>
                <Button transparent onPress={onShare} >
                  <MaterialIcons name="share" color={'blue'} size={24} />
                </Button>
             </FooterTab>
          </Footer>



          </Container>


    /*
      </PaperProvider>
    </SafeAreaView>
    */

  );
}


// https://nativebase.io/docs/v0.2.0/components#footer

/*
export default function App() {
  return (

    <Container>

        <View style={styles.container}>
          <SwipeCards />
        </View>
             <Footer>
               <FooterTab>
                 <Button transparent>
                     <MaterialIcons name="settings" color={'blue'} size={24} />
                 </Button>

                 <Title>Footer</Title>

                     <Button transparent >
                        <MaterialIcons name="share" color={'blue'} size={24} />
                     </Button>
                  </FooterTab>
             </Footer>
        </Container>
  );
}

*/



const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'black',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',


  },
  text: {
    color: 'white'
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    //marginTop: 22
  },
  leftAligned: {
    justifyContent: "flex-start",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalText: {
    margin: 10
  },

  scrollView: {
  height: '20%',
  width: '90%',
  margin: 5,
  alignSelf: 'center',
  padding: 10,
},
contentContainer: {
  justifyContent: 'center',
  alignItems: 'center',
  paddingBottom: 5
}

});
