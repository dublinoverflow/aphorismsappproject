import {Text} from 'react-native-elements';
import {StyleSheet} from 'react-native';
import React from "react";



const styles = StyleSheet.create({
  aphorismsItals: {
    fontStyle: "italic",
  },
});


function Aphorism1(props) {
  return (
    <Text style={[props.textcolor]}>
      <Text style={[props.textcolor]}>I told my little girl about death. She asked if I was joking. I said </Text>
      <Text style={[styles.aphorismsItals, props.textcolor]}>no</Text>, and she said <Text style={[styles.aphorismsItals, props.textcolor]}>I wish it was a joke </Text>
    </Text>

  )}


  function Aphorism2(props) {
    return (
      <Text style={[props.textcolor]} >
         <Text style={[props.textcolor]} >Every morning someone decides </Text>
         <Text  style={[styles.aphorismsItals, props.textcolor]}>no more.</Text> But not me. I have mouths to feed and mouths to shut the fuck up 
      </Text>
    )}

  function Aphorism3(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Why is it two o’clock? </Text>
        <Text style={[props.textcolor]} >my daughter asks. Oh, how I sputter</Text>
      </Text>
    )}


  function Aphorism4(props) {
    return (
      <Text style={[props.textcolor]} ><Text style={[styles.aphorismsItals, props.textcolor]}>Hey, you eating that? </Text>Again and again like the trill of the cicada horde
         <Text style={[props.textcolor]}>I keep asking, though the answer is often: </Text><Text  style={[styles.aphorismsItals, props.textcolor]}> Because fuck you </Text>
      </Text>
  )}


  function Aphorism5(props) {
    return (
      <Text style={[props.textcolor]} >Staring down the possibility of a dead son in my wife's belly through the long ride to the hospital, G-d seems to say <Text  style={[styles.aphorismsItals, props.textcolor]}>Smarten up. Your ideas are not good enough </Text></Text>
  )}


  function Aphorism6(props) {
    return (
      <Text style={[props.textcolor]} >I hold my breath to hold a thought. I say<Text  style={[styles.aphorismsItals, props.textcolor]}> eyes eyes </Text>
      just to watch</Text>
  )}


  function Aphorism7(props) {
    return (
      <Text style={[props.textcolor]} >Gangrene sets in quickly. Blessed nature is a billion-faced, poorly raised son of a bitch asking <Text  style={[styles.aphorismsItals, props.textcolor]}>Hey, you eating that? </Text>
      </Text>
    )}


  function Aphorism8(props) {
    return (
    <Text style={[props.textcolor]} >My dead friend and I say
      <Text  style={[styles.aphorismsItals, props.textcolor]}> I’m tired. I’ll see you tomorrow</Text>, both knowing we’re lying, but lying anyway
    </Text>
  )}

  function Aphorism9(props) {
    return (
      <Text style={[props.textcolor]}>New York Harbor lights up at dusk. And I want to say<Text style={[styles.aphorismsItals, props.textcolor]}> so sorry you didn’t get to see it.</Text> Then the fluorescent lights spring on to say<Text  style={[styles.aphorismsItals, props.textcolor]}> no one does, not really </Text></Text>
  )}

  function Aphorism10(props) {
    return (
      <Text style={[props.textcolor]} >
      We hear
        <Text  style={[styles.aphorismsItals, props.textcolor]}> bless you </Text>two, three times. Only later do we hear the sneezes</Text>
  )}



  function Aphorism11(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>A solar-powered graveyard</Text>, she said. I never found out if it was a damning verdict or an exciting business plan</Text>
  )}

  function Aphorism12(props) {
    return (
      <Text style={[props.textcolor]} >Assholes walked in acting like customers, saying <Text  style={[styles.aphorismsItals, props.textcolor]}>we’re being helped</Text>, as if they could be</Text>
  )}

  function Aphorism13(props) {
    return (
      <Text style={[props.textcolor]} >Each new invention is a wearisome thing, demanding <Text  style={[styles.aphorismsItals, props.textcolor]}>Redeem This!</Text></Text>
  )}

  function Aphorism14(props) {
    return (
      <Text style={[props.textcolor]} >The scavengers are brave and prettier than you might expect. To doom, they say <Text  style={[styles.aphorismsItals, props.textcolor]}>yeah I see you seeing me</Text></Text>

  )}

  function Aphorism15(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Serial killers and serial numbers and spree killers, shopping sprees, mass killers and critical mass</Text> — it sure sings like a square dance</Text>
  )}

  function Aphorism16(props) {
    return (
      <Text style={[props.textcolor]} >Five hundred portents, each one a helicopter, saying <Text style={[styles.aphorismsItals, props.textcolor]}>watch your step</Text>
      </Text>
  )}


  function Aphorism17(props) {
    return (
       <Text style={[props.textcolor]} >
          <Text  style={[styles.aphorismsItals, props.textcolor]}>Everyone and everything is trying to be cool</Text>, my girl explains 
        </Text>
  )}

  function Aphorism18(props) {
    return (
      <Text style={[props.textcolor]} >They call it<Text  style={[styles.aphorismsItals, props.textcolor]}>time</Text>, and we’re supposed to think it’s not thirsty for our warm blood</Text>
  )}

  function Aphorism19(props) {
    return (
      <Text style={[props.textcolor]} >
      What some folks call<Text  style={[styles.aphorismsItals, props.textcolor]}> culture </Text> feels like a gloved hand rubbing uncooked chicken on the pursed lips of infants</Text>
  )}

  function Aphorism20(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Let’s resample that well-recorded decade for another millennium</Text>. Every kingdom becomes a kingdom of frantic repetition
      </Text>
    )}

  function Aphorism21(props) {
    return (
      <Text style={[props.textcolor]} >
        These things we thought common as water are gone. A bar week is sixteen shifts—<Text  style={[styles.aphorismsItals, props.textcolor]}>that’s sixteen shifts gone</Text>. I’d mourn, but them I drink with are longer gone
      </Text>
    )}

  function Aphorism22(props) {
    return (
      <Text style={[props.textcolor]} >
        The last guy selling lingerie on Orchard Street will tell you: <Text  style={[styles.aphorismsItals, props.textcolor]}>The worst thing about success is that it does exist, somewhere else </Text>
      </Text>
    )}


  function Aphorism23(props) {
    return (
      <Text style={[props.textcolor]} >
        Tonight is rooftop drinks. Tomorrow morning is airport lines and <Text  style={[styles.aphorismsItals, props.textcolor]}>you must be at least this fucked to ride the ride</Text>
      </Text>
    )}

  function Aphorism24(props) {
    return (
      <Text style={[props.textcolor]} >Ever-busy, the dusty urethras and linty cunts of the non-apocalypse will always water down the booze and say,<Text  style={[styles.aphorismsItals, props.textcolor]}> don’t you mean to say...</Text>
      </Text>
    )}


  function Aphorism25(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Get Well Fast</Text> the card read. The face of a clock decorated its front
      </Text>
    )}


  function Aphorism26(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Birthday, Champion, President</Text>—the jive to make each year seem different starts to feel desperate
      </Text>
    )}

  function Aphorism27(props) {
    return (
      <Text style={[props.textcolor]} >I bang my damned shin on another grim, pink-granite extrusion of a civilization gathering up the nerve to say <Text  style={[styles.aphorismsItals, props.textcolor]}>okay this isn’t fun anymore</Text></Text>
  )}


  function Aphorism28(props) {
    return (
      <Text style={[props.textcolor]} >The glass-sheathed colossus is willfully invisible, like a father hissing<Text  style={[styles.aphorismsItals, props.textcolor]}> don’t look at me </Text>to a prostitute </Text>
    )}

  function Aphorism29(props) {
    return (
      <Text style={[props.textcolor]} >
        A woman carries the idea of
         <Text  style={[styles.aphorismsItals, props.textcolor]}> a pretty girl</Text> around her like a cloud of pepper spray
      </Text>
    )}


  function Aphorism30(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>There’s no such thing as life, Mom</Text>, I hear from a passing phone conversation </Text>
    )}


  function Aphorism31(props) {
    return (
      <Text style={[props.textcolor]} >Heads up: The <Text  style={[styles.aphorismsItals, props.textcolor]}>real</Text> in real estate doesn’t refer to reality</Text>
  )}


  function Aphorism32(props) {
    return (
      <Text style={[props.textcolor]} >I say <Text  style={[styles.aphorismsItals, props.textcolor]}>fuck it, I can live on my wits and $10 a day.</Text> Then I'm getting lunch at a cart, and a scabby-faced guy comes up and says <Text  style={[styles.aphorismsItals, props.textcolor]}>you’re a very rich man</Text></Text>
    )}


  function Aphorism33(props) {
        return (
          <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>We’ll email you a bill of rights</Text></Text>
  )}


  function Aphorism34(props) {
        return (
          <Text style={[props.textcolor]} >Everyone says the world is unfair. Then one person says <Text  style={[styles.aphorismsItals, props.textcolor]}>let’s see just how unfair</Text></Text>
  )}


  function Aphorism35(props) {
        return (
          <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}> Daddy, I make you special</Text>, she says Mere I am, she says, upon the sea. <Text  style={[styles.aphorismsItals, props.textcolor]}> Mere I am</Text>, but never to me</Text>
  )}

  function Aphorism36(props) {
        return (
          <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Tired of survival</Text>, people say quietly, if they say it at all</Text>
  )}


  function Aphorism37(props) {
    return (
      <Text style={[props.textcolor]} >The word <Text  style={[styles.aphorismsItals, props.textcolor]}>unbearable</Text>, when spoken, is usually a lie</Text>
  )}

  function Aphorism38(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>What matters</Text> has perhaps always been an illusion. But when the illusion gets truly threadbare, TV critics claim the force of the four horsemen</Text>
  )}

  function Aphorism39(props) {
    return (
      <Text style={[props.textcolor]} >Baseball introduces the word <Text  style={[styles.aphorismsItals, props.textcolor]}>consecutive</Text> to the boy, hinting at the silent triumphs of adulthood </Text>
  )}



  function Aphorism40(props) {
    return (
      <Text style={[props.textcolor]} ><Text style={[props.textcolor]} >Plan-B careers tend not to make sense. </Text><Text  style={[styles.aphorismsItals, props.textcolor]}>So your problem isn’t that you have to lie, but that you’re not in charge of the lies?</Text></Text>
  )}

  function Aphorism41(props) {
    return (
      <Text style={[props.textcolor]} ><Text style={[props.textcolor]} >We boys who traded in report cards for paychecks woke middle-aged, with lives bounded by pain we’d only describe as </Text><Text  style={[styles.aphorismsItals, props.textcolor]}>going back to those rooms</Text></Text>
  )}

  function Aphorism42(props) {
    return (
      <Text style={[props.textcolor]} >Maybe the TV listings had it right, and the real triumph is a mere <Text  style={[styles.aphorismsItals, props.textcolor]}>coming to terms</Text></Text>
  )}

  function Aphorism43(props) {
    return (
      <Text style={[props.textcolor]} ><Text style={[props.textcolor]} >The old men with strollers full of probably garbage and old women in sexy heels say: </Text><Text  style={[styles.aphorismsItals, props.textcolor]}>When I'm dead, bury me. Until then, figure it out</Text></Text>
  )}

  function Aphorism44(props) {
    return (
      <Text style={[props.textcolor]} >There are afternoons when G-d seems to ask, with genuine interest: <Text  style={[styles.aphorismsItals, props.textcolor]}>How real would you like it to be?</Text></Text>
  )}


  function Aphorism45(props) {
    return (
      <Text style={[props.textcolor]} >The only<Text  style={[styles.aphorismsItals, props.textcolor]}>gods</Text> we openly adore—if we elect those terms—are the conspicuously shitty gods</Text>
    )}

  function Aphorism46(props) {
    return (
      <Text style={[props.textcolor]} >People who speak of the <Text  style={[styles.aphorismsItals, props.textcolor]}>soul</Text> are too often trying to win an argument they don’t want to admit they’re in</Text>
  )}

  function Aphorism47(props) {
    return (
      <Text style={[props.textcolor]} >The August afternoon gets so hot that the words <Text  style={[styles.aphorismsItals, props.textcolor]}>November</Text> and <Text  style={[styles.aphorismsItals, props.textcolor]}>December</Text> sound phantasmagorical</Text>
    )}


  function Aphorism48(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Words aren’t for that</Text>, is the reason in the overheard conversation, the song from the passing car. No shelter, no lesson—not in <Text  style={[styles.aphorismsItals, props.textcolor]}>words</Text>, not for us, pal </Text>
      )}

  function Aphorism49(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>What was the future?</Text> they’ll ask. How we’ll laugh!
      </Text>
  )}

  function Aphorism50(props) {
    return (
      <Text style={[props.textcolor]} >He picks an asparagus from the countertop and snaps it open, looks inside. <Text  style={[styles.aphorismsItals, props.textcolor]}>Figures</Text>, he says</Text>
  )}

  function Aphorism51(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Feed the squirrels and you feed the rats</Text> reads the sign between luxury condo and riverine dump</Text>
  )}

  function Aphorism52(props) {
    return (
      <Text style={[props.textcolor]} >I try to poke an antenna up between diaper changes, client calls, and conversations with my wife that end with me saying <Text  style={[styles.aphorismsItals, props.textcolor]}>we’ll work it out</Text> over and over</Text>
  )}

  function Aphorism53(props) {
    return (
      <Text style={[props.textcolor]} >I go into the woods at night with a flashlight and call most of the forest the unlit part. Talk to me about <Text  style={[styles.aphorismsItals, props.textcolor]}>the unconscious</Text></Text>
  )}

  function Aphorism54(props) {
    return (
      <Text style={[props.textcolor]} >
        <Text  style={[styles.aphorismsItals, props.textcolor]}>Why’s it gotta be so hard?</Text> has two working answers. 1.) <Text  style={[styles.aphorismsItals, props.textcolor]}>It just does.</Text>2.) <Text  style={[styles.aphorismsItals, props.textcolor]}>It doesn’t.</Text> Anything else is pure fuckery</Text>
  )}


  function Aphorism55(props) {
    return (
      <Text style={[props.textcolor]} >Sauntering diagonal on horseback across the spine of night, the drunk asked us <Text  style={[styles.aphorismsItals, props.textcolor]}>Tienes mierda?</Text> It probably meant <Text  style={[styles.aphorismsItals, props.textcolor]}>do you have the shit?</Text> But we heard it as <Text  style={[styles.aphorismsItals, props.textcolor]}>do you have the fear?</Text></Text>
  )}

  function Aphorism56(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Are you afraid?</Text> flips to the question <Text  style={[styles.aphorismsItals, props.textcolor]}>Are you still willing to exist? </Text></Text>
  )}

  function Aphorism57(props) {
    return (
  <Text style={[props.textcolor]} >People sprang into the streets to protest the police, to challenge the <Text  style={[styles.aphorismsItals, props.textcolor]}>use of force</Text>, as we called it then</Text>
  )}

  function Aphorism58(props) {
    return (
      <Text style={[props.textcolor]} >I read Bible stories to your sister at night. She liked how <Text  style={[styles.aphorismsItals, props.textcolor]}>all the stories knew each other</Text>, and I said, <Text  style={[styles.aphorismsItals, props.textcolor]}>yes, they knew us, too</Text></Text>
  )}

  function Aphorism59(props) {
    return (
      <Text style={[props.textcolor]} >My Massachusetts accent unfurled at the mention of <Text  style={[styles.aphorismsItals, props.textcolor]}>G-d.</Text> I preferred it to <Text  style={[styles.aphorismsItals, props.textcolor]}>lord</Text> —an old revolutionary aversion to calling anyone, even the author of the universe, <Text  style={[styles.aphorismsItals, props.textcolor]}>lord</Text></Text>
  )}

  function Aphorism60(props) {
    return (
      <Text style={[props.textcolor]} >A voice from the sidewalk calls out <Text  style={[styles.aphorismsItals, props.textcolor]}>salami</Text></Text>
  )}

  function Aphorism61(props) {
    return (
      <Text style={[props.textcolor]} >Price tag, shoe name, boat profile, hair style, minor skyline—the sun sings <Text  style={[styles.aphorismsItals, props.textcolor]}>it’s all more to see, more to see</Text>, and tramples the optic nerve</Text>
  )}



  function Aphorism62(props) {
    return (
      <Text style={[props.textcolor]} >Boat shoes and sandals for showered ankles in the early evenings when the dads realize <Text  style={[styles.aphorismsItals, props.textcolor]}>oh, it happened to me too</Text></Text>
  )}

  function Aphorism63(props) {
    return (
      <Text style={[props.textcolor]} >The baby gives a look that says: <Text  style={[styles.aphorismsItals, props.textcolor]}>I am the entire universe and you are messing with me</Text></Text>
  )}

  function Aphorism64(props) {
    return (
      <Text style={[props.textcolor]} >There are afternoons when G-d seems to ask, with genuine interest: <Text  style={[styles.aphorismsItals, props.textcolor]}>How easy do you want things to be?</Text></Text>
  )}

  function Aphorism65(props) {
    return (
      <Text style={[props.textcolor]} >A voice flashing silver and orange says <Text  style={[styles.aphorismsItals, props.textcolor]}>good luck with your poetry</Text></Text>
  )}

  function Aphorism66(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Only G-d can pick up Daddy </Text></Text>
  )}

  function Aphorism67(props) {
    return (
      <Text style={[props.textcolor]} >Saying that <Text  style={[styles.aphorismsItals, props.textcolor]}>everything is just fine</Text> is a weapon sometimes </Text>
  )}


  function Aphorism68(props) {
    return (
      <Text style={[props.textcolor]} >I asked the trees <Text  style={[styles.aphorismsItals, props.textcolor]}>How can you go on, baiting an implacable force?</Text> They answered with one voice <Text  style={[styles.aphorismsItals, props.textcolor]}>One day, we’re going to strangle that bastard sun </Text></Text>
  )}


  function Aphorism69(props) {
    return (
      <Text style={[props.textcolor]} >Confusers <Text  style={[styles.aphorismsItals, props.textcolor]}>would</Text> hide their tons of gold under Liberty Street </Text>
    )}


  function Aphorism70(props) {
    return (
      <Text style={[props.textcolor]} >
       <Text  style={[styles.aphorismsItals, props.textcolor]}>Free free free</Text> — for another five minutes—promises the bus carcass and busted water tower</Text>
     )}


  function Aphorism71(props) {
    return (
      <Text style={[props.textcolor]} >The trench-deep gutter of autobiography—<Text  style={[styles.aphorismsItals, props.textcolor]}>I sold my soul for a pack of gum cause of the sorry place I’m from</Text>—is what some folks expect when you flinch</Text>
  )}


  function Aphorism72(props) {
    return (
      <Text style={[props.textcolor]} >To the celebrity on the sidewalk: <Text  style={[styles.aphorismsItals, props.textcolor]}>I go to reality to get away from you people</Text></Text>
  )}

  function Aphorism73(props) {
    return (
      <Text style={[props.textcolor]} >The lesson from tonight’s beggar: <Text  style={[styles.aphorismsItals, props.textcolor]}>Don’t laminate your pain</Text></Text>
  )}

  function Aphorism74(props) {
    return (
      <Text style={[props.textcolor]} >Some folks despise every truth, law and opinion, except the voice that says <Text  style={[styles.aphorismsItals, props.textcolor]}>Yes, you deserve more pornography</Text></Text>
    )}


  function Aphorism75(props) {
    return (
      <Text style={[props.textcolor]} >From the angel-avalanche above the church door; the insignia that spans public and intimate sections of a woman’s thigh; the interlocking <Text  style={[styles.aphorismsItals, props.textcolor]}>H</Text>s of the parking lot, another literacy beckons</Text>
  )}

  function Aphorism76(props) {
    return (
      <Text style={[props.textcolor]} >Why’d you do those things? Now replace <Text  style={[styles.aphorismsItals, props.textcolor]}>pressure</Text> with <Text  style={[styles.aphorismsItals, props.textcolor]}>pleasure</Text>, then try it the other way</Text>
  )}


  function Aphorism77(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Believe</Text>, the singer says, <Text  style={[styles.aphorismsItals, props.textcolor]}>that you have just about all the time in the world </Text></Text>
  )}


  function Aphorism78(props) {
    return (
      <Text style={[props.textcolor]} >The North American continent is littered with landscapes humming <Text  style={[styles.aphorismsItals, props.textcolor]}>this is still okay</Text> to a tune that has stopped playing</Text>
  )}

  function Aphorism79(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>How would outer space help?</Text> asks every book about outer space</Text>
  )}

  function Aphorism80(props) {
    return (
      <Text style={[props.textcolor]} >I spend my days conceding candy-colored promises and barking imperatives: <Text  style={[styles.aphorismsItals, props.textcolor]}>Go; stop; eat; sleep</Text></Text>
  )}

  function Aphorism81(props) {
    return (
      <Text style={[props.textcolor]} >The fruit of an evening’s shamanic foray:  <Text  style={[styles.aphorismsItals, props.textcolor]}>They did something terrible to us, and now we’re their responsibility </Text></Text>
  )}

  function Aphorism82(props) {
    return (
      <Text style={[props.textcolor]} >The train-facing graffiti ads and murals blur to <Text  style={[styles.aphorismsItals, props.textcolor]}>WHOWILLSTEALFROMYOUANDHOW </Text></Text>
  )}

  function Aphorism83(props) {
    return (
      <Text style={[props.textcolor]} >In the commercial, the only onscreen obscenity is committed against   <Text  style={[styles.aphorismsItals, props.textcolor]}>perfectly good cold cuts</Text>. But the implications are explicit</Text>
  )}

  function Aphorism84(props) {
    return (
      <Text style={[props.textcolor]} >Is there anything more intangible than the word<Text  style={[styles.aphorismsItals, props.textcolor]}> tangible?</Text></Text>
  )}

  function Aphorism85(props) {
    return (
      <Text style={[props.textcolor]} >An unwelcome wisdom greets the question <Text  style={[styles.aphorismsItals, props.textcolor]}>Why would such a wise man lie to us?</Text></Text>
  )}

  function Aphorism86(props) {
    return (
      <Text style={[props.textcolor]} >
      <Text  style={[styles.aphorismsItals, props.textcolor]}>Daddy wins the trophy of G-d and then he gets stepped on</Text></Text>
    )}

  function Aphorism87(props) {
    return (
  <Text style={[props.textcolor]} >The holiday cinches the hose. But the pressure presses. In my girl’s tiny mouth, <Text  style={[styles.aphorismsItals, props.textcolor]}>Jingle Bells </Text> becomes <Text  style={[styles.aphorismsItals, props.textcolor]}>single G-d</Text></Text>
  )}

  function Aphorism88(props) {
    return (
      <Text style={[props.textcolor]} ><Text  style={[styles.aphorismsItals, props.textcolor]}>Free </Text>reads the pink post-it on the oversize can of peaches in the crook of the old woman’s arm</Text>
    )
  }



var defaultFontColor = {'color': 'white'}

// why can't this just be an array of all components? why are we doing this object prop thing?

var Aphorisms_array = [ {text: <Aphorism1  textcolor={defaultFontColor} />}, {text: <Aphorism2 textcolor={defaultFontColor} />}, {text: <Aphorism3 textcolor={defaultFontColor} />}, {text: <Aphorism4 textcolor={defaultFontColor} />}, {text: <Aphorism5 textcolor={defaultFontColor} />}, {text: <Aphorism6 textcolor={defaultFontColor} />}, {text: <Aphorism7 textcolor={defaultFontColor} />}, {text: <Aphorism8 textcolor={defaultFontColor} />}, {text: <Aphorism9 textcolor={defaultFontColor} />}, {text: <Aphorism10 textcolor={defaultFontColor} />}, {text: <Aphorism11 textcolor={defaultFontColor} />}, {text: <Aphorism12 textcolor={defaultFontColor} />}, {text: <Aphorism13 textcolor={defaultFontColor} />}, {text: <Aphorism14 textcolor={defaultFontColor} />}, {text: <Aphorism15 textcolor={defaultFontColor} />}, {text: <Aphorism16 textcolor={defaultFontColor} />}, {text: <Aphorism17 textcolor={defaultFontColor} />}, {text: <Aphorism18 textcolor={defaultFontColor} />}, {text: <Aphorism19 textcolor={defaultFontColor} />}, {text: <Aphorism20 textcolor={defaultFontColor} />}, {text: <Aphorism21 textcolor={defaultFontColor} />}, {text: <Aphorism22 textcolor={defaultFontColor} />}, {text: <Aphorism23 textcolor={defaultFontColor} />}, {text: <Aphorism24 textcolor={defaultFontColor} />}, {text: <Aphorism25 textcolor={defaultFontColor} />}, {text: <Aphorism26 textcolor={defaultFontColor} />}, {text: <Aphorism27 textcolor={defaultFontColor} />}, {text: <Aphorism28 textcolor={defaultFontColor} />}, {text: <Aphorism29 textcolor={defaultFontColor} />}, {text: <Aphorism30 textcolor={defaultFontColor} />}, {text: <Aphorism31 textcolor={defaultFontColor} />}, {text: <Aphorism32 textcolor={defaultFontColor} />}, {text: <Aphorism33 textcolor={defaultFontColor} />}, {text: <Aphorism34 textcolor={defaultFontColor} />}, {text: <Aphorism35 textcolor={defaultFontColor} />}, {text: <Aphorism36 textcolor={defaultFontColor} />}, {text: <Aphorism37 textcolor={defaultFontColor} />}, {text: <Aphorism38 textcolor={defaultFontColor} />}, {text: <Aphorism39 textcolor={defaultFontColor} />}, {text: <Aphorism40 textcolor={defaultFontColor} />}, {text: <Aphorism41 textcolor={defaultFontColor} />}, {text: <Aphorism42 textcolor={defaultFontColor} />}, {text: <Aphorism43 textcolor={defaultFontColor} />}, {text: <Aphorism44 textcolor={defaultFontColor} />}, {text: <Aphorism45 textcolor={defaultFontColor} />}, {text: <Aphorism46 textcolor={defaultFontColor} />}, {text: <Aphorism47 textcolor={defaultFontColor} />}, {text: <Aphorism48 textcolor={defaultFontColor} />}, {text: <Aphorism49 textcolor={defaultFontColor} />}, {text: <Aphorism50 textcolor={defaultFontColor} />}, {text: <Aphorism51 textcolor={defaultFontColor} />}, {text: <Aphorism52 textcolor={defaultFontColor} />}, {text: <Aphorism53 textcolor={defaultFontColor} />}, {text: <Aphorism54 textcolor={defaultFontColor} />}, {text: <Aphorism55 textcolor={defaultFontColor} />}, {text: <Aphorism56 textcolor={defaultFontColor} />}, {text: <Aphorism57 textcolor={defaultFontColor} />}, {text: <Aphorism58 textcolor={defaultFontColor} />}, {text: <Aphorism59 textcolor={defaultFontColor} />}, {text: <Aphorism60 textcolor={defaultFontColor} />}, {text: <Aphorism61 textcolor={defaultFontColor} />}, {text: <Aphorism62 textcolor={defaultFontColor} />}, {text: <Aphorism63 textcolor={defaultFontColor} />}, {text: <Aphorism64 textcolor={defaultFontColor} />}, {text: <Aphorism65 textcolor={defaultFontColor} />}, {text: <Aphorism66 textcolor={defaultFontColor} />}, {text: <Aphorism67 textcolor={defaultFontColor} />}, {text: <Aphorism68 textcolor={defaultFontColor} />}, {text: <Aphorism69 textcolor={defaultFontColor} />}, {text: <Aphorism70 textcolor={defaultFontColor} />}, {text: <Aphorism71 textcolor={defaultFontColor} />}, {text: <Aphorism72 textcolor={defaultFontColor} />}, {text: <Aphorism73 textcolor={defaultFontColor} />}, {text: <Aphorism74 textcolor={defaultFontColor} />}, {text: <Aphorism75 textcolor={defaultFontColor} />}, {text: <Aphorism76 textcolor={defaultFontColor} />}, {text: <Aphorism77 textcolor={defaultFontColor} />}, {text: <Aphorism78 textcolor={defaultFontColor} />}, {text: <Aphorism79 textcolor={defaultFontColor} />}, {text: <Aphorism80 textcolor={defaultFontColor} />}, {text: <Aphorism81 textcolor={defaultFontColor} />}, {text: <Aphorism82 textcolor={defaultFontColor} />}, {text: <Aphorism83 textcolor={defaultFontColor} />}, {text: <Aphorism84 textcolor={defaultFontColor} />}, {text: <Aphorism85 textcolor={defaultFontColor} />}, {text: <Aphorism86 textcolor={defaultFontColor} />}, {text: <Aphorism87 textcolor={defaultFontColor} />}, {text: <Aphorism88 textcolor={defaultFontColor} />}, {text: "It’s hard, at first, to spot the shimmer that becomes your life"},
{text: "My daughter says her dreams always start in the middle, like how the year begins on a Wednesday in the middle of a pay period in winter  "},
{text: "I will try not to abuse the privilege of your attention"},
{text: "My wife puts away Christmas tree ornaments with a tenderness that proves something no calamity can disprove"},
{text: "The outcome is always necessarily in doubt"},
{text: "I’ve sung and suffered, hidden in thickets of whisper treaties and climbed salmon ladders of degradation. I fear the time is near when I must speak plainly or die dishonest  "},
{text: "Hurry up and tell the story before poetry can suffocate it"},
{text: "The glittering skyline is made of failure and dirt "},
{text: "Try to avoid an adventure, and misadventure rears up"},
{text: "The glittering skyline is propaganda. You can feel your own salesy effort when you try to appreciate it "},
{text: "My agenda? I'm like that victim on the news—critical but stable"},
{text: "Reality is a fountain"},
{text: "Listen for the song sung with air that wants no song sung into air that wants no song sung "},
{text: "If you’re breaking a promise you never made, at least you’re not making a promise you can’t keep"},
{text: "Look at your hands, front and back. They’re a symbol beyond language, reminding"},
{text: "Hurry up. Tell it before the devil knows your drink, before the president knows your name"},
{text: "G-d’s favor or disfavor often speaks to me through a woman’s body"},
{text: "Tired, I’ll agree we all need a story like a bird needs a nest or a fox needs a burrow"},
{text: "Like a tightrope walker don’t look down. Like a magician don’t say how"},
{text: "I try to feel the way someone else looks. But they catch me, and try it back"},
{text: "How I look is not how I feel is not what I’m for"},
{text: "Something matters. For us lacking subtlety, death seems a clue"},
{text: "A pigeon or a dozen eggs intercept me at my doorstep to remind me I haven't been forgotten "},
{text: "Having not forsaken the ill-worn proposition of my own specialness, I locate an unsurveilled grove and try to claim the full privilege and price of a human being "},
{text: "Be dazzled and aroused. Recall the belonging to which you were invited by a fleeting image"},
{text: "Sometimes you don’t know what you think until you start talking"},
{text: "Something older than childhood beckons—the calendar stone of Boston, the soap that blinks with the faces of the ancient unborn"},
{text: "Snakes own the diamond mine where men go to get rich. The men spend their wages on food to replace the blood the snakes drink"},
{text: "No sooner had mankind had its first idea than it had the wrong idea"},
{text: "Pursuing a chirp heard before I hatched, I wander the earth"},
{text: "Some days are a highwire made of burning bullshit, spun by no one so trustworthy as a spider"},
{text: "It may never be as hard as this ever again. And when it’s over, I’ll miss it terribly   "},
{text: "The tyrant-parasite and the true savior eventually come to an uneasy accommodation - something only a catastrophically broken heart could broker"},
{text: "I play nice all day with baby. But G-d comes at night, and He wants a rematch"},
{text: "Little nothings of light ripple along the crippled turnpike on the morning of the taxi strike"},
{text: "He fell off the highwire and now he’s somebody’s dad. He fell off the highwire and now he’s the fault line in somebody’s heart"},
{text: "All I ever saw of the unborn one was a butterfly of brown blood on the sheets "},
{text: "Today was before, and will resume as a moonbeam in a sweat bead, a wink from the smear trail of the street sweeper in diagonal morning light"},
{text: "The inexhaustible promise of morning defeats me for breakfast"},
{text: "One day, the cliff face will release the drill marks of the dynamite that exposed it to the highway"},
{text: "You get to choose, or not choose, if you decide"},
{text: "A dream, a song—then off to the maelstrom of weird abuses, to the shit we have to eat to even be alive "},
{text: "Things that should matter forever vanish in a murky backwash of minutiae. Even the devil helps the aggrieved, sometimes"},
{text: "The sun throws us to and fro more than we can summon the intellect to imagine"},
{text: "I was tutored in New York scripture, dense and treacherous"},
{text: "Welcome say the Janus-faced vaginas of the Brooklyn Bridge"},
{text: "Every airy fantasy is woven on a loom of violence "},
{text: "Red-bottomed boats in the harbor ignore the glossy office aquarium towers of momentarily necessary monsters"},
{text: "The hardest way to keep your balance is to stand still"},
{text: "You’re working a bad job. And I’m here for a drug that helps me work a slightly less bad job. That’s better than a lie, right?"},
{text: "The avenue at rush hour is full of predation dressed as pleasure and vice versa. Survival and entertainment are the least of what is happening"},
{text: "They think about the ingredients of the langostino salad and the price of palladium. They don’t think about you"},
{text: "An octagonal bucket connects the Hugh Carey Tunnel to the pearlescent empyrean, offering up black traffic mist to a hungry nostril"},
{text: "The bowl of the horizon curves like the shins of a newborn "},
{text: "Perhaps for G-d to say one coherent thing to you, He’d need to begin speaking six thousand years ago"},
{text: "The shrapnel of forgotten wars still fly in the necktie, the glare of a windshield, and the half-laugh at the misheard word"},
{text: "Nature is a gargantuan snooze. But you’ll lose your mind if you don’t sleep "},
{text: "Like time and space, I’m a dubious interlocutor"},
{text: "So much love, desperation, work and deep joy goes into one more spiritual false start"},
{text: "So much love, desperation, work, and deep joy goes into becoming more junk on a screen"},
{text: "On stylish streets, people sharpen the syntax of what they mean, only to give away their real life stories by accident"},
{text: "Death is a grand tradition, like pursuing what you don’t honestly desire"},
{text: "Depends on whether or not I see my shadow. Hate that guy"},
{text: "Daily responsibilities are trauma enough at a certain pitch of consciousness "},
{text: "The goblins of exile grind out typos all day long"},
{text: "The gimmick and the ruin can stand still. My eyes dance like bees"},
{text: "Metaphors cross contaminate. Words form the mouth"},
{text: "At the end, we discover the apocalypse was an overture, with every over-stylized villain we’d later become already present at the original divorce "},
{text: "The story that makes the entire continent of reality into an obstacle. It presses through every friend "},
{text: "Going to and fro across the earth, rolling and tumbling and crying, I was crowned like a street or like a shit, given a form that’s hard to accept"},
{text: "Into a drama of rental cars and adulterous near misses, a crack of lightning split the cosmos in New Hampshire"},
{text: "Poets climb the mountain of purgatory by night"},
{text: "That I was a flicker in a void, making up stories; That my vocabulary was incomplete, and so I was free to speak. Who would tell me these things?"},
{text: "It’s a bootstrap operation. You do it from despair, on faith "},
{text: "Work is a kind of sleep—a quarter dream, half of one discomfort"},
{text: "A glimpse of men wrestling chickens into gray plastic garbage cans at the slaughterhouse to start each morning has a stabilizing effect on the mind"},
{text: "The average person heard what you were saying about him, and he’s pissed"},
{text: "There’s a pungent irony when you argue on behalf of a fuller reality"},
{text: "There are dead spots in the facade. Not every creditor will even break a nose. The biggest bullies rely on ribbons and bows "},
{text: "Wealth is poison and poverty kills"},
{text: "Peasants muster by the tits of reality—ever so full of strategies! "},
{text: "Because the airport appears real and I am ever in doubt"},
{text: "To the dumpster, to the throne—we were all chased from somewhere"},
{text: "Some speech is more than free. It costs the listener"},
{text: "Success has a thousand fathers. Failure is an orphan. The day is the bastard son of a thousand maniacs"},
{text: "Some folks say you get what you get. But that gets to be bullshit, too"},
{text: "Wealth immolates into a million ineffectual devotions to calisthenics, aesthetics and anesthetics"},
{text: "I said I slipped, but that was a lie. I threw the fight, sold out to peripheral interests for the proverbial short money"},
{text: "We eat out every night. Side dishes hem in our cosmos"},
{text: "There is little trust and everyone works as some form of babysitter"},
{text: "The gift shops are full of halos and the delis are full of sins. It’s a grand time to be flat, flat broke "},
{text: "If we keep on striving, the ornate shadows of starvation are all we’ll ever know "},
{text: "No one wants anything within a mile of whatever the fuck you’re about? Take solace!"},
{text: "This is what the future looks like when the future doesn’t come again and again"},
{text: "Summer comes. Suddenly everyone has the same secret"},
{text: "You can see your reflection in a grimy, faded orange traffic cone. But don’t"},
{text: "During the time when you might think anything at all, every shadow might house an eye or a successor"},
{text: "The gaze of strangers will nourish and trouble you. But you can’t starve untroubled, either"},
{text: "Rotund pudendas of shield faces, bandoliers of egg and dart: The transmission tower of reality is usually a little ridiculous "},
{text: "The withdrawal of religion feels like the withdrawal of a hormone"},
//{text: "—it sure sings like a square dance"},
{text: "The ritual—once called a human being—obsolesces"},
{text: "How we conspire in the everlasting fire!"},
{text: "The first thing that ties you to the ever-foundering age is a riff, a dimple, a trifle. The second is usually a betrayal   "},
{text: "What does the deli look like after five thousand years?"},
{text: "A shadow—contagious as a yawn or rising prices—squats on our brains like a goblin"},
{text: "The spots where sex intersects with religion intersects with politics are uneasy on the eye. Minerva’s nipple peeks from the rim of her shield—deadly!"},
{text: "Let’s talk recreation"},
{text: "I checked my phone for the weather, and found every fifteen minutes of fame—yours, mine, the Babylonian Empire’s—made wildly interchangeable"},
{text: "When something doesn’t feel quite right, don’t spend every last cent on it"},
{text: "Entertainment shifts to a straight analgesic. We argue over the daydreams we can be trusted with, and prove we can be trusted with precious fucking little "},
{text: "World Wide Whip—wield it or have it wielded upon you"},
{text: "I was there when the money was spent—every dollar an arthritic knot in a back-twisting labor or mind-bending sleight—sent to oblivion with glee!"},
{text: "There’s a new blurriness to the present—a draft from fore and aft"},
{text: "One of us is an asshole. We’re trying to get through the night without finding out who"},
{text: "I am no longer a cool spirit flitting among quaint and curious volumes of forgotten lore, but a consumer of broadband. They say I agreed to it"},
{text: "The time to pretend it is another time has run out"},
{text: "Maybe I’ve lost my mind, or maybe I just feel the teeth of the tick that’s drinking my blood"},
{text: "Memories blur to echoes of action figures of reveries of pictures of anecdotes of movies… The copier gets sloppier "},
{text: "Inside a gigantic book that reads itself, why do or say anything? "},
{text: "I lose such chunks of my humanity arguing with fools who don't exist "},
{text: "A new tower ten miles off pokes through the avenue’s treetops. Built as sold, it’s tall, and that’s all"},
{text: "An engine holds up the walls, and a pill lets them drift"},
{text: "The crisis tightens its grip, but loses its force. The tip of the breaking wave seems to boil as it grows thin"},
{text: "Beside the great salt lick of real, verified existence, love was never going to be enough"},
{text: "The wake of the boat looks the same as the inside of a fish"},
{text: "The hero wears his medals to bed. The film director insists every naked soul at the orgy learns his name "},
{text: "You see yourself like you see the strangers in the airport—benefited by one too many showers and a pre-owned sense of self-regard"},
{text: "Total responsibility seizes some folks until they marvel that a butterfly might flutter past a thornbush and not apologize for being a butterfly"},
{text: "You can acquire boundless admiration behind a mask, but to harvest a single drop of love, you must remove it"},
{text: "My meal is more markup than food, paid for in my own markup, folded into someone else’s markup. What could go wrong?"},
{text: "There’s the cat and what you choose to ignore about the cat. Together, they make a cat—hopefully one you like, and feed"},
{text: "For those who demand the judgment of G-d, there are slot machines"},
{text: "I still get mail in the addled, unmopped places where they saddle angels with human faces"},
{text: "The jogger and the insulated window are still poor symbols. But just wait"},
{text: "In the beachhead and graveyard of dreams, the rents never go down"},
{text: "I spend a short walk trying to explain verse and chorus to my daughter—the mirth at becoming dismayed, and the delight coming back"},
{text: "Who wants to go the distance just to see what the judges say?"},
{text: "There’s a valley where, at night, the unicorns turn to vampires "},
{text: "The receptionist said the messenger had nice teeth. For us, this was reason to be afraid. But the elders saw it as the weakness it was"},
{text: "The devil is without inner reflection, completely reflective in appearance, like a modern office building. Contract-employee demons line its gut"},
{text: "Sleep is the penultimate mother"},
{text: "Man named the animals, but G-d numbered the days"},
{text: "The radio plays another song about a man who wants to do good, but just can’t take it another second"},
{text: "All the damage bore the signature of builders"},
{text: "I climbed Gold Flamingo Hill to the casino-hospital, and searched for the right bartender to overtip to see a doctor"},
{text: "I punched the mortal timeclock, but my astral penis was at the store"},
{text: "You go gunning for the devil, but what gets you is some dumb kid, some trivial habit, some spot blind enough to pass for G-d"},
{text: "Bodies and souls; angels and devils—each believes they’re conning the other. It’s what cosmology comes to in an age of media"},
{text: "The hopeless cases will fight to stay in hell"},
{text: "Oppressed by the satisfactory, I wear a prisoner’s spit-proof hood curated by the world’s finest chefs"},
{text: "It was never much to look at, but it was a place a person never entirely leaves"},
{text: "Screaming serves a couple purposes. It slows the breath, which wants no part of the mess you’re in, and which you’re going to need"},
{text: "Dreams renegotiate the borders and treaties between ourselves and skin cancer and steel, corn flour, our grandmothers and Jupiter"},
{text: "You’re a dream, too. You just happen to be a well-fed one"},
{text: "The Lower East Side was a jamboree of survival. A lot of people got hurt in ways that would take generations to unwind"},
{text: "Intellectual honesty doesn’t care what I want. The humanity I smudge from another also comes out of my own account"},
{text: "Any ad buyer or copywriter will tell you, there are no heroes in media"},
{text: "A certain tide pounds the jetties of sleep"},
{text: "Engraved in the locket of my heart are the perpetually unimpressed expressions of women who grew up too poor to be afraid "},
{text: "My daughter has a keen eye for the unhappy actions buzzing all around her. So I teach her history, one treason at a time"},
{text: "Some folks say reality isn’t so much a fountain as a toilet in need of flushing "},
{text: "Michael O’Momps leads the gang in a lengthy chat about what their last names were supposed to have been"},
{text: "People at the funeral are dressed for when they had different bodies "},
{text: "If you can keep alone in it, a crowd can be nice—a current against the bait "},
{text: "Between the G-d you trust in and the one you’ll admit to, mind the gap"},
{text: "I’m also from a lesser American city that carefully pines for the bad old days when it was better loved "},
{text: "I’m also from a lesser American city in its one hundredth second act—pleading to be plowed by plastic suitcase wheels "},
{text: "An archipelago of shipwreck realities is where you must begin"},
{text: "Never marry a corpse, no matter how accomplished"},
{text: "Born again? I’m still digging out from the first one"},
{text: "I’m also from a lesser American city stranded in interest rates and oversized televisions where lazy-minded faces argue who’s owed a Super Bowl"},
{text: "The horizon is marked by the stamp of whoever could keep a straight face that century"},
{text: "Who will add a warble to the cold chirp of culture?"},
{text: "In the act of dying, what new will there be to see?"},
{text: "Beyond the bosses who torment in service to electricity and water, there’s a wilderness that’s deaf to us whenever we pause from killing for even a moment"},
{text: "Name for me the rivers that separate compulsion from habit from hobby from economic necessity"},
{text: "It’s hard to know who to root for here in America"},
{text: "G-d is a bandage on a wound so large as to beggar imagination and bankrupt belief"},
{text: "No matter how damned you may be, spurn the automated blessings"},
{text: "People on rooftops, maybe ugly and maybe crippled, kiss"},
{text: "Like every light, you fled to the bushel"},
{text: "Quarter to five in the morning, the birds and church bells are horny and loud"},
{text: "There’s a troll who’ll fix your crystal pony if you bring him the fingers and toes of your peers. You start with the bad kids, but soon run out "},
{text: "Big city, big jobs—all fun and games, but to bang your chin on the parallel bars is death itself"},
{text: "The cameras get too good, and we all must pay"},
{text: "Nine feet high, the graveside flowers formed a vodka bottle"},
{text: "The mote in my eye is a million years old. It’s killed millions and sustained millions more"},
{text: "Lovely, ugly New York may yet forge its charter to the crackling dawn of time"},
{text: "Entertainers never give a straight answer. And the entertained have the look of well-corralled prey"},
{text: "Solutions metastasize to disasters faster than they did last year "},
{text: "I’m not the first or the last guy to try hiding under the devil’s feet"},
{text: "Make a habit of living at close quarters with them who mean you harm, and soon you can’t live without them "},
{text: "The witches have reasons: candy or bone marrow or because the museum misspent its patrimony. But at night they turn the townfolk into statues "},
{text: "Luxury luxuriously enforces itself with the nondescript music of a waiter gently collecting the check and clearing out a dangerous customer "},
{text: "I don’t always love how I’m treated. But I get it "},
{text: "The upscale mall is an airport for the luxuries we would be "},
{text: "After everything, people still wave at boats"},
{text: "I am subject of the realm, subject to the whim of more than I care to admit"},
{text: "The light is influenced by lady-sneezes that pile every consonant against half a vowel, by the way an obscure fabric falls across a belly or buttock"},
{text: "Beside the thousand-dollar belts, a security guard explains to a diagonal-faced junkie couple that they will have to leave"},
{text: "A civilization in which no one ever turns down the money is a sad wet fart"},
{text: "Among the well, one must act as if one belongs"},
{text: "In the company cafeteria, a colleague warns about a plague of madness among the middle-aged    "},
{text: "G-d is not vain. He will show up wearing hand-me-downs, even yours"},
{text: "Even a spaceship is boring if you’re paying for it"},
{text: "The melody’s in custody. The sailboat and helicopter deliver time up for execution"},
{text: "In full view of the rain, a fountain bursts and burbles "},
{text: "Unhappy are the estuaries where the stories we tell about ourselves mingle with the stories we tell about others"},
{text: "The yachts are laced with lights and spaced like a valedictorian’s knees. Obnoxious beauty pouts in obvious cages. The atrium is a turbine of misunderstood energy"},
{text: "The day is like a shrapnel ricochet—its source and trajectory obscure and frightening"},
{text: "It’s hard to miss the bone-deep difficulty inscribed within so-called recreation "},
{text: "The sidewalk hustlers invoke the fiscal year"},
{text: "Awash in a vast, poison voice that needs—with the force of a trillion dollars and a million finer futures foreclosed—to be heard, how shall you live?"},
{text: "Sometimes I think you people actually like all this cancer"},
{text: "The time fills on the landfill. A shy-faced tourist constantly nearly reveals her breasts"},
{text: "Merchandise and biomass on the avenue splits like a zipper at the hobo negotiating a new covenant from the double yellow line"},
{text: "Every year, it gets harder to imagine words that will do more good than harm"},
{text: "We professionals live in the shadow of nearly becoming victims to the arrogance we enjoy"},
{text: "Some blend of current, crosswinds and customs keeps the saint at sea"},
{text: "The tide is tired"},
{text: "The words show the invisible: a modern office building, a molten orifice budding  "},
{text: "Complexity creates dependency. Beware vampires in these parts"},
{text: "The horror of being addicted to something other than French fries and happy talk "},
{text: "Consensus is a desert. But you need to go to an actual desert to see that sometimes"},
{text: "The spectacle is a relief. Any story we can share is a refuge from our private burrows"},
{text: "The man who’ll give you an enemy implacable enough to protect you from yourself is the best friend you ever had "},
{text: "Head swapped in by thrifty sculptors, eyes washed of witness—the ever-troubled prophet and the ever-present chopper-off of the prophet’s head"},
{text: "But there is a certain mind that forgets nothing and forgives less. There is a certain mind"},
{text: "Maybe we can’t understand ancient Egypt because we can't understand our own civilization"},
{text: "An opiate must be joyless for it to be endemic"},
{text: "The shadows chuckle"},
{text: "Time acts like money, piling up in vaults. Money acts like time, going away"},
{text: "Hurry past the reflective curtain wall of the momentarily profitable "},
{text: "Who can take responsibility for the sins and abuses that birthed them? Who can take responsibility for the functions of their liver and kidneys?"},
{text: "Session expired. Forty more years of usernames and passwords. Secrets successfully modified. No cost, except you want to live a little less afterwards"},
{text: "There’s a semi-visible monster under the streets and you can either die trying to face it or die trying to pretend it’s not there"},
{text: "There’s usually two or a hundred ways to do something that are easier than making it good "},
{text: "Sandwiched between the airplanes and plumbing in a leviathan’s smile-frown, I feel less like a strident negotiator than the terms of someone else’s settlement"},
{text: "The challenge with any bounty—of time, talent, money, or beauty—is how to best squander it with manifestation"},
{text: "Are the singers ripping themselves off? Is a tree ripping itself off when it puts forth a flower?"},
{text: "A laxative paradox, indeed. But who can relax for long enough to ponder it?"},
{text: "The omnipresent tyrant-celebrity is the same but different each week: a pre-failed talisman against the dark night of the soul"},
{text: "Try to shoehorn your talent into the charred crust of the world"},
{text: "To mass-produce yourself and go to sleep seemed the best way to cope with eternity. Publishers and cameras appeared—common and cheap as sand "},
{text: "The plane’s wings are always nipping the tree branches"},
{text: "The craftsmen embellish the surviving idols with the skins and regalia of the ones we just euthanized"},
{text: "There will always be ample hats and T-shirts for the people who can never belong to enough armies"},
{text: "Just when your expectations of the world reach their lowest, it’s time to meet the unbounded expectations of your son. This crossing is the site of a miracle"},
{text: "Workaday days and nights are fine food for the photosynthesis of misprision "},
{text: "A wild oak grows tangent to the tilt and thrust of reality "},
{text: "Everyone agrees this is a fallen world. But what it’s fallen from is baffling past discussion "},
{text: "Reality’s a feckin disaster, a chaos of chapstick, tobacco and gum kisses, followed by the acid discharge of septicemic feelings "},
{text: "Demoralized by contemporary life? Try another fucking compromise!"},
{text: "The ashes of tomorrow drift into our caviar"},
{text: "In a bird/angel language, my infant son describes the lake and river systems of the northeast "},
{text: "You never know who’s gonna wanna settle a score with eternity "},
{text: "The dollars press to disproven release within an engine running on unintended consequences."},
{text: "Safety is a hallucination based on wishing-well capital projections"},
{text: "It’s no coincidence that something so shallow and dubiously alive as a virus should undo us"},
{text: "Then came the year we failed at all the shit we probably never should've been doing in the first place "},
{text: "Was it a field of stars or the coiled scales of a black serpent?"},
{text: "The first blossom bounces silently on the subway’s linoleum floor "},
{text: "As the plague descends, Sikh prayers whine from the liquor-store TV"},
{text: "The Thing You Thought You Saw—Yes, It Meant That"},
{text: "The city’s theophany goes occupying-army quiet"},
{text: "Throughout this reasonable duration in the best of all possible worlds, it’s always going to be a crisis, either A La Carte or BYO "},
{text: "Everything, to a baby, is an antique "},
{text: "No one’s ready for now. Never have been"},
{text: "A spiritual problem, turned from the door, dresses as an epidemic to get in—Trojan horseplay at its finest"},
{text: "There's no good outcome for a balloon"},
{text: "Metaphors jump the blood-brain barrier and kill you just the same "},
{text: "Under permission of the synonym, asterism swaps for constellation, lips swap for a bear swaps for a spoon. The speaker begins to sense just what they’re up against"},
{text: "Go to where it is unbearable. Go to where you’re unbearable. That’s all the exercise I’d recommend"},
{text: "Big earphones became respirators we had to wear to buy wine through plywood windows"},
{text: "Empty F trains gleam as they cross the Circumferential Parkway"},
{text: "Deferred costs come due in a foam of bankruptcies like the million fluttering eyelashes of the windy commuter train platform—on schedule"},
{text: "The moon repeats herself in step with forgetfulness, and makes a new moon "},
{text: "No one said the jubilee would be so frightening"},
{text: "To die young in bank marketing is hideous and cruel"},
{text: "The resale price of the obvious never seems to benefit me"},
{text: "To hear of such a thing as a well-recorded 169,000-year astronomical cycle will leave a mark on you"},
{text: "It was a beautiful bummer—a time and place where being wrong was as easy as opening your mouth"},
{text: "Liars come in choirs"},
{text: "Putting nuts into shells is a bad job"},
{text: "Washing the sugar off pills is a bad job"},
{text: "Grinding rubble into sand is a bad job"},
{text: "Either historical fiction or it’s science fiction: There are no other genres for this ridiculous year "},
{text: "Who can say what time it is? Every savior makes it their business model to disagree"},
{text: "My compatriots flee with enough food and soft-faced idols to last the night"},
{text: "The river of wood spreads through a continent of air and vanishes into the thirsty sky "},
{text: "Would you speak as if no one’s ever said a word? Could you?"},
{text: "There’s no place for your unique identity in traffic"},
{text: "The wilderness of Abraham is alive and well in the eyes of strangers and the trees at dusk"},
{text: "The act of washing dishes is but a deeper kink in the labyrinth that an exploding star makes"},
{text: "Then I wake up one Tuesday, awash in the mercy of not being too late for anything"},
{text: "She looks like her dad, if he was as pretty as her mom"},
{text: "The marshals seized the storefront where my daughter pondered puppies "},
{text: "My daughter and I went out by the sunflowers and the parking lot to drink the air"},
{text: "Reality is playful, but so hard to get to"}, 
{text: "A trillion atoms over a trillion years will only find their ways to sit side by side to watch this TV show one single time, unless there’s a rerun"},
{text: "Take good care of that garbage truck "},
{text: "When your employer is a flesh-eating bacteria, hitting those deadlines will only get you so far"},
{text: "Go looking for a weather forecast and ten thousand ransom notes pour in"},
{text: "There’s always work digging subbasements under whatever hell you’re in"},
{text: "Grabbing at a hand from a passing train—I never guessed that would become a fantasy of stability. But here we are"},
{text: "A People’s Oral History of Easy-to-Watch TV: Every day leaves us fragile and unforgiving"},
{text: "We turn on each other, almost always for something embarrassing"}, 
{text: "There will always be folks who say it was better when the lizard people ran things"},
{text: "The heart is no warm 60-watt incubator. It’s a thresher. The heart… watch out! "},
{text: "There are rooms where no one can do anything quite right, where it’s nonetheless hard not to blame yourself "},
{text: "Scratches in a notebook and creases under eyes in an otherwise pretty face, with tiny numbers beside each line like a countdown to Knowing Better"},
{text: "The birds and the bees are frisky between bankruptcies"}, {text: "When the angel reveals her fullness, you’d be wise to lower your chin and make a fist"},
{text: "Turns out the people are the hoax. It’ll blow over soon enough"},
{text: "Permission to be stupid, sir!"},
{text: "Penmanship, sculpture and gift wrapping are only a few of the delicate acts from which I have recused myself"},
{text: "What gets you is the echo"},
{text: "A poor man’s possessions multiply like fractions as they get fewer. The room-for-rent photo in the flyer will never be entirely free of debris"},
{text: "G-d talks to every thief. But only the best ones know not to listen for too long"},
{text: "Something matters here in the city. It’s the ball under one of these three cups"},
{text: "Everyone wants something for nothing. So the smart folks invent new nothings of even less substance"},
{text: "I spent an entire, terrible, pointless, sleepless night trying to reset myself to a diamond"},
{text: "The puzzle doesn’t fill in. It expands"},
{text: "Money, naturally, gets tired—almost as tired as it makes us"},
{text: "The money always gets tired before it can explode. But some people only ever want to explode"},
{text: "I heard about the butterfly wings that started a hurricane and said I’m gonna get me a thousand of them butterflies "},
{text: "My father calls and tells me he can no longer ascend a stepladder to change a lightbulb  "},
{text: "The bombers that streak above the joggers are just another charm on the afternoon’s bracelet"},
{text: "The brook’s respectable cousin is a canal"},
{text: "The music doesn’t drone or joke or malinger. It only intensifies or speeds away  "},
{text: "The Boss can spot a malingerer, especially a talented one. You can outrun and outpunish him, but only for a while "},
{text: "The cash register behind your face isn’t as natural as some folks like to say"},
{text: "Only years later did we learn about the terrible affliction suffered by the bullet that killed the president"},
{text: "The wall of disdain between innocent people and the people who sinned to buy their innocence is a load-bearing wall"},
{text: "They call him a rich man, but he's just expensive to be around "},
{text: "There’s tension in a standing structure. A tower is a kind of bomb"},
{text: "Nothing works all the time. And sometimes, nothing works at all"},
{text: "No advice. Fuck advice "},
{text: "The tabletop trembled as the trucks passed. The landscape did more with less"},
{text: "Natural and unnatural advantages mingle in the flying buttress of hips, portcullis of pelvis, aluminum shoulders and pigeon-proof eyebrows"},
{text: "You can hold out against the evidence of the senses, but only for so long"},
{text: "You need an ace in the hole that can survive the acid current of jokes you need to stay free"},
{text: "Every shadow starts in the sun"},
{text: "The sunny meadow of humanity revealed by a mandolin must also become a tomb, simply by dint of all the things it isn’t"},
{text: "In the end, I’m pitting demons against demons to nurture something that has never existed"},
{text: "But when the demons bubble down—what a foul calm!"},
{text: "Small-town snow stains the brown yellow slopes on a string of sunny false-spring days"},
{text: "Every liberation is also a trap. Buck, laddy buck. Buck!"},
{text: "They call it a business, but it’s a for-profit totalitarian cult run by people who know the secular never really was "},
{text: "The television fixes me in its blind primary-color eye with a letter or number inside and says I’ve been communicated with "},
{text: "Nothing provokes quite like an absence"},
{text: "One day, they will ask you to sign up to die. Only then will they break out the beauty"},
{text: "Some folks get their kicks swearing up and down that we’re all sinners who spent all the money we never even had. I never got why they like to say things like that"},
{text: "You open the door, innocent, and step onto a sidewalk the color of cooked sausage "},
{text: "A weird poverty rises as material conditions improve"},
{text: "My path vanishes in the underbrush of potted plants killed by Frank Sinatra’s martinis "},
{text: "The in-flight magazine explains an ancient metropolis like it’s a familiar piece of software "},
{text: "Some folks own the copyright on the lie they told you, so you can’t remind them of it"},
{text: "The executioner’s tits are impeccable"},
{text: "Spring comes on with contradictory weather, like a liar’s mix of apology and threat"},
{text: "Anyone can lash out, but who’s damaged enough to sustain an assault?"},
{text: "I too am often too eager to take my cues from the worst that’s been done to me"},
{text: "The library is a no-holds-barred streetfight for understanding and for joy"},
{text: "The first guy to run a marathon died young as a junkie, a little younger than an alcoholic. He died on the job"},
{text: "When you suspect that what you love is ultimately nothing, you start playing footsie with nothing’s foxy cousin, death"},
{text: "The tremendous advantage of a mistake is that you’ve already made it"},
{text: "Speech and sex are higher excrements, and the cement of cities"},
{text: "Hooray for fun"},
{text: "Original impulses recede. Kluges span the gaps. Bills come due. Cost eclipses vision. Altar for sale by owner"},
{text: "Rising prices are the paper of the news: the near-murder, the divorces and half-orphans, the promises people could only afford to make"},
{text: "What? Admit I’m a decaying bag of historically discredited meat with a third-rate education and no legitimate epistemological basis for speaking aloud? No thanks"},
{text: "Being one thousand dollars less fucked is worth being one million dollars more rich"},
{text: "Cargo in tall stacks drifts humbly from the meadow of the bay. Under the threat of distant, incredible violence, we arrange flowers "},
{text: "From the hand-curated, free-range nirvanas arises a new damnation: to be able to change how you feel with the caress of a sensor"},
{text: "The skies get so empty that a passenger jet sounds like a snowplow "},
{text: "Boats float on fog and the boardwalk fills with sand. Time and the ocean eventually make a lie of everything "},
{text: "To the everlasting shame of us talkers, the listeners outlast us"},
{text: "The past is a sibling conjoined to everything I see"},
{text: "Like divorced parents, the only thing the sun and moon have in common are us kids"},
{text: "A great sucking undertow pulls the plywood and paste from what we had for a golden age"},
{text: "I felt what I was for. I took a look at how I felt"},
{text: "What it’s for is not how it feels is not how it looks"},
{text: "Who changed the bedrock of the human heart more—Jesus or electricity?"},
{text: "In America when it is America, the best and worst live cheek by jowl "},
{text: "Ambition is a risky way to understand "},
{text: "No one is as frenetic as a person dancing to a song that’s ended"},
{text: "Pointless is the land. Pointless is the child"},
{text: "There’s an unseemly slowness to the boardwalk procession, like the tire pressure had already dispersed into the avenue of death"},
{text: "Through the cracked plate-glass window of the kohanim room it’s hard to tell if the lights are on or if the roof caved in"},
{text: "Judas, Judas, if you’re able, keep your elbows off the table"},
{text: "Dip your nickel in the precious blood"},
{text: "Forty-three with a kiddo on my case, a second on the way, and a Gypsy curse on my sunglasses"},
{text: "Checks from the New York Times and New York State Unemployment. Halfway through any given sentence I forget if I’m bragging or begging for help"},
{text: "Summer rolls up the crooked coast. Quarantine curlicues of donut rubber mark the intersections. Wind rattles the roll-down gates"},
{text: "In a stranger’s downcast glance, you can see what it’s like to nurse a sprained ankle in your soul for a whole life"},
{text: "Alternating between science and blame, between early deaths and health regulations eschewed—I mumble in time with the moment"},
{text: "Grave marker and boardwalk, garbage fires in the park and robot bones in calzone alley. The calendar doesn’t care. No one cares. Only you care "},
{text: "Alleyways swing past in parallax, and for a moment there’s a clear way through, a straight shot"},
{text: "The Spanish roof of the hilltop basilica passes for a school bus in the sky"},
{text: "Police, fire, sanitation, EMT—utility and symbol start to blend"},
{text: "How it looks is not what it’s for is not how it feels"},
{text: "Pretty for strangers: Clean and simple, so they could draw your face with just two or three lines "},
{text: "The way we drink our time—it costs"},
{text: "Just because I have khakis on doesn’t make me the politburo for an absurd reality"},
{text: "Summer nods its head. The whole earth sweats and begs for crime"},
{text: "Sudden delight of the sea in view! Old people past the breakers, past the bad habits of their genders, wading, arms bent and raised like wings that never materialized"},
{text: "We crowd the barricades to become less than ourselves, to become material in a comprehensible design "},
{text: "Another soaring anthem makes you complicit in a scheme you otherwise wouldn’t touch"},
{text: "We don’t only live, but serve, like the half-sunk barges and the fishermen when you approach Coney Island from behind"},
{text: "Our America was built on jokes"},
{text: "Kelp-tangled Neptune bellows from a face upon a tumbledown amusement warehouse. Whatever he’s bellowing, I bet it keeps the world from collapsing"},
{text: "Ol’ Zeus is a wilted fruit in the summer sun. His open shirt flaps farewell to the passersby"},
{text: "It’s so hot that the only way to move is to sit very still and let time handle the hauling"},
{text: "Mesopotamia translated through Scotland and an immigrant-stew house of horrors is a funny way to approach Jerusalem, a funny time to bring this up, a funny guy, but ain’t we all?"},
{text: "A trillion screams tune each guitar string"},
{text: "Mired in drycleaners, liquor stores, bakeries—how shall such a layabout son of reality see?"},
{text: "The sky is full of minutes. But you can only get to them with your mouth"},
{text: "It feels so serious, but looks so comical"},
{text: "The end result was an aerodynamic hobo with digital tears—all the options "},
{text: "Each year the dead get more dead, until you round an invisible corner and start walking toward them"},
{text: "Death imposes its persnickety parallax death on every event"},
{text: "Forms distort for a moment and everything comes back to everything, with a fucking wink"},
{text: "The years melt homes and monuments to just moments"},
{text: "How it looks is not how it feels is not what it’s for"},
{text: "The places we met were less than places. But we were never less than ourselves"},
{text: "I fumble to get my mask up in time to tell people on the street that they disgust me and that I’m nobly saving their lives"},
{text: "An artist? Or a salesman rubbing a sick self against the nation’s health?"},
{text: "The prison and the shoals of homeless are not unintended consequences, but an ancient and ascendant point"},
{text: "I, too, often find myself taking the side of cruelty. I too often find myself taking the side of cruelty"},
{text: "Some folks will say that they’re an accident, just so they can say you’re one too"},
{text: "Work distracts you from mortality distracts you from work "},
{text: "Private quibbles and ghoul kibble—maybe they’re poison. Maybe everyone is"},
{text: "Neither cruel nor vindictive, she arrived early in the place of adulthood, with older boys—caterpillar mustaches and varsity jackets"},
{text: "Corsage for her wrist, boutonniere for my jacket, dancing, hands on waist or shoulders, chaste but touching, laughing, the differences deepening between songs "},
{text: "A thousand tricks buoyed us from childhood to parenthood—hairspray, liquor, cigarettes, mouthwash, cheap thrills and dirty jokes"},
{text: "A life doesn’t extend like a line. It inflates evenly like a bubble. No matter the dimensions or duration, it is entire"},
{text: "Pimples dimpled the skin. Did the water ripple then?"},
{text: "A life insurance nurse comes to the house to put my pimpled Irish ass on their spreadsheet—another pork belly on the exchange"},
{text: "The pressure transmutes an individual to a commodity, then to a sensation "},
{text: "The frontiers that kept us sane are unreliable"},
{text: "Go ahead—argue with the barber. See what that gets you"},
{text: "We live in a residue, perhaps as a residue, cussing our luck and plotting vengeance on what has long since forgotten us"},
{text: "Latex underwear and Cobra health insurance: the amenities of exile"},
{text: "We all got problems. Just ask the mailman’s podiatrist’s psychiatrist’s nutritionist’s mailman"},
{text: "The problem calls for professional agonizing. So I hope you have time. No one who bills by the hour is a true enemy of nonsense"},
{text: "What monuments do we build when we know we will have been wrong? Lazy ones"},
{text: "The light by which one sees does not itself see. So, you’re free"},
{text: "By the forty-second Armageddon you start to take the hint"},
{text: "Each step along the low, low road will feel as if it was your own idea. Perhaps that’s a mercy"},
{text: "Deli shelves fill with MREs for the war on boredom and the war on yourself during a long, noisome summer when one war bleeds into the next"},
{text: "It’s bedtime. I break a piece off of a child’s toy to stir my drink"},
{text: "American cities burn on the news. I spend the afternoon with mismatched screws, building a crib"},
{text: "I stalk the ice cream truck up the fringe of the park. I argue with my daughter about bedtime, argue with my wife about whose turn it is to worry"},
{text: "Forty-three—I could’ve said the thing, or caught the bullet, or stopped the bulldozer, or the bill. Something. But I didn’t "},
{text: "Those days wouldn’t go fast enough, but expired with uncontrollable swiftness. I guess they’re still like that"},
{text: "It seemed that beauty had obsolesced, even as a weapon"},
{text: "Any makeshift mercenary mistruth can become numinous when perceived by us who’re numinous"},
{text: "I lost my job. I wrote books, and the slimy junk that paid. I took your sister on my bicycle in loops around the shuttered golf course"},
{text: "There were murders done in the name of law, order, skin and property. It was an awful abuse, and I hope you find it awful still  "},
{text: "Your mother and I sweated and cursed the world, but never each other. We kept it close—a family grows into a cult of sorts"},
{text: "There’s no survival without faith—in each other, in the future, and trailing behind is the ever-faltering faith in ourselves. We cleaned and cooked and worked"},
{text: "We did everything that was asked—but we had different voices asking us "},
{text: "Maybe it was those Manhattans, or just to keep her from interrupting, but I read those bedtime stories loud"},
{text: "It’s a lot of work, getting the house ready for a baby. A lot of work for a dad, newly unemployed, with hopes unfulfilled and goofy. A lot of work for a mom, rightly worried "},
{text: "I had my schemes—a sitcom about a graveyard. Sorry about that. I lacked a nose for what people wanted, or I just didn’t want such a nose "},
{text: "You’ll see why, or maybe you won’t—G-d knows what kind of dad I’ll be to a son, and what that’ll do to you"},
{text: "The wars call out like unanswered questions. On a breezy summer day, we eat chips by their memorials, confounding and fulfilling their hopes"},
{text: "Take the loophole, and then loop home"},
{text: "Though we lived in view of the LaGuardia, Newark and JFK flightpaths, we saw one or two airplanes a day"},
{text: "I try to say what I mean, but all the words are peasant verities or medieval kluges or sound effects keep a drowsy customer awake—a thousand years past their inspiration dates "},
{text: "Those plague days were like a dream, little details askew by a few degrees"},
{text: "It seemed everything was about to change—explode, perhaps—and for real this time. But I’d felt that way since I was sixteen. You’ll see, I hope "},
{text: "A baby is born. And now I’ll never truly be a stranger again. Every ribbon ties, every bow crosses its arms and widens its eyes"},
{text: "Sunrise over the farthest Rockaway jetty, and Manhattan still a palace of night—desolate and askew "},
{text: "Birth and death crowded together unabashed in the operating room. For a moment, the reality of being alive exceeded every condition or purpose laid upon it"},
{text: "I was a grownup then, handling diapers and automobiles, driving through the longest tunnel from the hospital to our home in the shadow of the biggest bridge "},
{text: "Such tunnels and such bridges! New York rich enough to regret the sea!"},
{text: "Regret foams like the edge of a towering wave, shines like the surge that incinerates the filament"},
{text: "White light flares in the bathroom. Blood slops on the floor. New schemes anchor and liberate the almighty soul"},
{text: "Protestors in the park press their advantage. The bystanders hope it’s simple revenge. Into it all, comes a baby"},
{text: "The baby looks around, more hungry than impressed "},
{text: "We test every surface. The diners dine until dined upon. Dancers dance until danced upon"},
{text: "The baby opens his eyes here and there to track the voices. He has no questions. It’s all self-evident to the baby"},
{text: "A crescent moon hangs above the timid restauranteering of a pandemic summer night full of inconsequential fireworks"},
{text: "We can be so many things, under one single name"},
{text: "For daddy and baby alike, the adventure looks like squirming, mostly"},
{text: "The more moving parts, the more that can go wrong. We advance in step with calamity "},
{text: "Every maelstrom of desire matures into a quaint small town"},
{text: "I do not know. And yet, life and death seem to hinge on acting as though I do"},
{text: "Into it all comes a baby. It all comes into a baby  "},
{text: "She handled the baby and I handled the road, while the baby handled something else—a job I know, and an important one, though I don’t know its name "},
{text: "A crying child filibusters me from interminable debates. The ad for an exercise machine ceases to be a threat to my legendary sanity"},
{text: "I spend wordless days in the gentle expectant breathing of a watched animal "},
{text: "There are a trillion dead chickens in each practical idea. But the outcome still depends on the astrological position of garbage herders migrating toward a rumor of six-cent redemptions"},
{text: "At the middle of the maelstrom, what’s wrong with the world becomes what’s right with it"},
{text: "Blue veins throb in a newborn’s temple and a mother’s breasts. At night, blue lightning flashes in the clouds "},
{text: "The capillaries of unending punishment flow through time, invisible until it’s too late  "},
{text: "The infant with a pacifier looks like Gabriel with his horn"},
{text: "I wonder how anyone imagined such a thing as open space or simple form, then I see the shock the first time a milkfed baby drinks cold water "},
{text: "Don’t forget cherubs and spare ribs, bagel stores and dinosaurs"},
{text: "My little girl makes colorful bookmarks for the books she won’t let me finish"},
{text: "A paper wall offers polite isolation from an otherwise deadly universe "},
{text: "You sketch to commemorate when the subject best threatened sit still, or just when it best resembled something"},
{text: "With lights, cranes and sound effects, men make believe"},
{text: "The notebook goes in a shopping bag, crumpled with still more lines, each a minuscule kick in the gonads"},
{text: "Imperfection finds us—the sooner the better"},
{text: "The only time the cat won a staring contest in its twenty-one years was right before it died"},
{text: "Summer’s promontory is an old military concern sold as a tonic to the vainglorious work camps of the Eastern seaboard. All the streets end in ocean"},
{text: "Only later do we learn what the madman did with the brook and the beehive"},
{text: "For all our talk of dominion and damnation, the crickets don’t seem to have noticed"},
{text: "The bears hide, but none are as depressed as the ones they hide from"},
{text: "Past the ice cream shop, shuttered for the evening, the ocean spreads upwards—another, lesser sky stacked up until morning"},
{text: "Tri-state dads are indistinguishable from one another on the beach. Bellies like bellows, they bark about saltwater"},
{text: "Tri-state wives save the box tops and cut the coupons of eroticism"},
{text: "Sometimes playing dumb is the smart move. Sometimes being dumb is the smart move I say I’m a mountain, but I’m a sandbar, a barrier island. The hoteliers may shell out to shore it up another season, or they may not"},
{text: "Someone is dying. Someone eats their first arancini "},
{text: "Knowledge is a whirlpool. I can see its tightening bottom but can’t imagine where it empties out. I can’t imagine surviving the journey "},
{text: "A baby’s spit-up bleaches the wine stain from an undershirt"},
{text: "To kick against the pricks is only cute when you’re a rising prick"},
{text: "The docks are crowded with wooden chests and plastic coolers of words. Some lads carry past a shark cut lengthwise in quarters"},
{text: "Every business is import-export. We each make our living in someone else's subliminal"},
{text: "Reality has always been largely unprofitable "},
{text: "Borders dissolve and the import-export business pivots to the obfuscation economy"},
{text: "Every shark feels a sensation adjacent to pain when the fin emerges from the water "},
{text: "Nightmares made of nightmares construct a nightmare—a mire of spittle wire, a tangle in Medusa’s hair on a humid night, a home"},
{text: "The shark may have been a killer, but he was always nice to me"},
{text: "I waste hours trying to see what I saw, to feel what I feel, to say what I feel"},
{text: "Clouds travel over our dinner. Broken storm bones seek another storm in which to fuse and to break"},
{text: "A clue to life on other worlds appears from the blood in the breastmilk and the first diaper after the circumcision"},
{text: "It’s so insane what makes up the best days of our lives"},
{text: "My baby boy kicks his own wounded penis. It’s a nightmare, a dream come true"},
{text: "The scream teases a new note into the afternoon"},
{text: "The joy of a child’s birth isn’t simple. In it lies the joy of agreeing to perish"},
{text: "Rain pastes leaves to a funeral home’s awning. Wind comes next, but when?"},
{text: "The last young man outside the Eden Farms minimart to put his shirt on will necessarily believe himself possessed of a special destiny"},
{text: "Mourners and new parents keep equally odd hours"},
{text: "I hold the crown of your head under mine like a violin. The plates of your skull expand, finding the seams along which they’ll seal"},
{text: "Halos of alluvial sand shoal the landing gears of reefers parked a year in front of the storefront funeral home  "},
{text: "I stay up late with you and sing a lot—mostly about milk and the virtues of going back to sleep"},
{text: "Cold weather found me—breast milk in my thin hair and talk of pony crimes on my lips—yawning with awe, yawing with awe"},
{text: "The balloon’s pop brings the hard truth of time close enough to kiss. The child’s whole body rebels against it, tries to dry-heave it away"},
{text: "It was a golden age of walking into walls. Key rings filled the streets. Strangers left them on standpipes, top bolts of hydrants and base ridges of lampposts"},
{text: "Summer poaches the clouds like eggs. The moon is a melting pat of butter"},
{text: "A billion blushing angels color each red brick. What a thing—to walk down the street!"},
{text: "Bush-league suns peek from the folds of a garbage dump"},
{text: "Archons and guardian angels, aliens and ghosts, Frankenstein and Christ revived are not creations, but holes punched in a dark box, like they said stars were "},
{text: "Wadded-up week-old pleas from the canned goods aisle make the fire purr tonight "},
{text: "Gingerly walking my infant son down the narrow hallway back to his crib, I feel the eyes of my ancestors upon me. In his bedroom, the baby monitor chirps with feedback"},
{text: "Pain is the port. The vast ambiguous galaxy of joy is the storm"},
{text: "Fifty cents of butterfly wheezes, a billion dollars of sunlight—mind-altering substances abound "},
{text: "More Adventures of a Likeable Character: The creative guy cooks nice dinners, makes up games with his daughter, surprises his wife. That’s all"},
{text: "Ten years married, I still smell my shirt before putting it on"},
{text: "Doing your best—especially if you fail—is an act of respect toward what matters and an act of hope for those to come"},
{text: "Dreams presume to say what your life means. Then you presume to say what it means. It’s a jig around the treacherous flame"},
{text: "I balance on the seam where accidents stop happening and where they start again"},
{text: "Save whatever you have to. But don’t save the world"},
{text: "This Tiresias has radial tires for eyes"},
{text: "Blind force would bury even the memory of sight beneath the flat flesh of the brow "},
{text: "No one can keep the day of their death from being referred to as a Wednesday, or a Thursday, one of those fuckers"},
{text: "I had to blind myself to nuance to stay kind "},
{text: "Swaddled in diagonal stripes, head askew in the direction of a dropped pacifier, my boy is the image of a whirlwind falling carelessly asleep"},
{text: "My son. I see you new, and yet yoked under a curse adjacent to my own"},
{text: "Someone catches you looking into their blood. A ripple runs through them "},
{text: "No one can teach you what you actually need to know—how to be hit in the face and have it not matter"},
{text: "The question of my father must remain open in order to hold ajar the question of myself "},
{text: "A Juniors cheesecake for our wedding anniversary, and so our daughter can end her day with something sweet"},
{text: "A jagged poignancy snares the lightning strike "},
{text: "Do not underestimate the degree to which your freedom is someone else’s opportunity "},
{text: "The plane’s rear hatch opens onto a sled trail down to a foamy sea. As you glide to the beach, the crowd cheers"},
{text: "I invoke the right not to find out until it’s too late"},
{text: "The messages in bottles bobbed back to shore. It was the ocean, asking if we planned to give back all that wind "},
{text: "The test in middle age is to be buried alive and not complain"},
{text: "The only one who will tell you anything is free is a salesman"},
{text: "A woman seems assembled from frontiers and coastlines. A man seems a lazy gonad in a complicated splendor "},
{text: "The hope in which I act is tea-stained the color of people recklessly sainted by the daguerreotype—brown bruises in pale fat "},
{text: "Rain rain rain—even the sun is rain. Time is rain. It’s a rainy night. Come to the city. See the great plans, cooked in rain "},
{text: "There is no smooth, dry side of a junkie upon which to adhere the logo decal"},
{text: "Freedom expensively bought isn’t much freedom at all"},
{text: "I’m lucky no one listens when I claim responsibility for the traffic, the rain and the curse"},
{text: "Freedom, like the Camaro, is a promise we can only muster the energy and the stupidity to believe for a short while"},
{text: "Older than rock n’ roll or Dionysos: To deliver your species’ most vital truths, and eschew survival. What a spiritual discipline! Who wouldn’t want to hear the results! "},
{text: "Another health, another death"},
{text: "Sadists and masochists take turns on a single wheel, weathered beyond tread"},
{text: "You never know where the last straw will find you"},
{text: "The breakdowns at an awards ceremony are split equally among winners and losers—not much among the staff, busy plating cakes and touching up cracks in the plastic crowns"},
{text: "Citizens and customers behave differently, see differently, matter differently and die differently "},
{text: "It’s the kind of night when everyone accuses everyone else of being a movie star in the Reade Street Pub. My motives are as obscure as theirs"},
{text: "Just ask an unscrubbed summer-school bully, and he’d tell you if he could: Neurosis is the best way to control something smarter than you"},
{text: "Standing at the door of the train, a woman leans back to welcome the morning rush. Static electricity draws a few strands of her long hair to my short-cut crown"},
{text: "Death is vanishingly difficult"},
{text: "Good luck to us spawn and apostles of The Promised Cardiac Arrest "},
{text: "How do people deal with the certainty of death? Some better than others "},
{text: "A personality is an unstable combination of how it looks and how it feels. By the time it appears, it’s too late to ask if it’s voluntary, or what it’s for "},
{text: "Like Odysseus weeping bitterly one four-star morning-after with Calypso: It’s not what he wanted, but he sure did stay a while"},
{text: "Rage is a reliable holiday from doubt"},
{text: "Your hat says something that your plastic shopping bag disputes"},
{text: "Pretending sanity, I swore my enemies were outside myself "},
{text: "You call him president. I call him a man who’s found his foolhardy way into the very belly of a whale. And whales have no president"},
{text: "The professionals inflame an ancient hurt. You see a salesman do it, and you start to despise the artist "},
{text: "No president has ever been adequate to explain why I’m an asshole, or why you are"},
{text: "I picked a side and got boxed in as bad as the last asshole. My aspirations became excuses, because I let everyone down. No, I did"},
{text: "Stay positive. Eat around the mouse’s tail in the popsicle"},
{text: "You’re only strong enough to kill me, and that’s all you’ll ever be"},
{text: "New York City in those days had the atmosphere of a cordoned-off escape"},
{text: "Absolute father—the weight of his gut rounds his shoulders down and in—ready to slip into reality and never be heard from again "},
{text: "They buried him before things could get any worse"},
{text: "Those are the good guys? I’ll see you folks later"},
{text: "Young man full of sun/Old man crammed with vouchers "},
{text: "Rare is the progress not reeking of ambush"},
{text: "I used to strut the library. But one day, I noticed I couldn’t hold more than seven digits in my mind at a time. It was scary. It was a reason to forgive"},
{text: "The only way to tell the story of your job so someone will listen is to couch it as flattery or make it a prelude to payment"},
{text: "The harbor smells of the ocean one day and an unventilated toilet the next. How can anyone honestly claim to love anything?"},
{text: "Angels taunt the clouds and the saints who see them into existence"},
{text: "In the realm of delight, if you’re not ruthless, you’re not credible"},
{text: "So much of the day comes down to finding the thoughts that’ll open or close the doors to dreaming"},
{text: "Monopile donut fender; sprinkler system synopsis; poor people with expensive strollers—each small thing changes the meaning of life"},
{text: "The clouds part for the Jehovah-cliche sunbeam to show you just where in Jersey to dig"},
{text: "Can of beer, Camaro gearbox and the hand that could be counted on to hit—you just want something that has something to do with you"},
{text: "Desire begins everywhere, supersedes itself ceaselessly"},
{text: "Pissed off, coiled like a walnut or flaring like a hoop from the surface of the sun: serpents always crushed, always whisper"},
{text: "Then there’s another body—arguments insinuated in garments—another urge, another bureaucracy of fantasy"},
{text: "I conjure so much browbent focus because I can’t even hold onto unhappiness "},
{text: "A tide buzzes the bees, blossoms the buds and varnishes the verisimilitude. But my foot still grips the sand sometimes"},
{text: "When things get too woolly-pully, honesty is accidental, marked by malapropisms and unintended meanings "},
{text: "Though warned, you wore your nice shirt into the brambles of time’s own fruiting and decaying monsters "},
{text: "The mirror I call untrue even as I negotiate with it—it does hold my attention"},
{text: "He didn’t want her more than anything—just more than he wanted to think his own thoughts "},
{text: "What’s universal is often attended by some cautionary stink"},
{text: "The love songs on the jukebox can be sung addressed to G-d, then sung as G-d, then back again"},
{text: "When you feed the body to the mind, certain illnesses are inevitable"},
{text: "I’ve got to keep this guy happy. And as you can probably tell, that’s no mean feat "},
{text: "The true crest of the city is a bright purple lipstick mark on a black plastic coffee-cup lid on a public bench outside the Salvation Army  "},
{text: "Personal tastes being what they are, few are attractive to everyone. But even fewer are unattractive to no one"},
{text: "Ears like oyster shells: Yaquina little, Shigoku big. The line from chin to earlobe shows the way"},
{text: "Silence is an egg squeezed between warm knees. Water splashes. Every edge warms with negotiation. There is no calm solution"},
{text: "How it feels is not how it looks is not what it’s for"},
{text: "When fantasies lose their perversity, beware of collapse"},
{text: "The body alone in the bar—was it pretty or merely appropriate? "},
{text: "A flowering father commits his tulip atrocities"},
{text: "At sunrise, birds accuse each other of having trespassed into the timed treasure vault of existence "},
{text: "You never want to see inside of someone’s body. The problem is that sometimes you think you do"},
{text: "Wound up, alright. But like a watch? Or like an apple stem?"},
{text: "The game consists of nearly missing a careless woman’s womb"},
{text: "But one day, winning feels like busting up a junkyard"},
{text: "Bumps on a hastily shaven pudenda and an asshole like a shiny penny. Good luck to you if you call it lucky "},
{text: "Nothing is anything until it’s too much"},
{text: "Sometimes solace is disgusting "},
{text: "Secret thoughts on awnings and marquees accuse me of my next treachery"},
{text: "Clothed in sleepy clitoral hoods of resentment until stirred to destructive frenzy, we wait our turn to dare a void of sprain "},
{text: "Tell it terrible"},
{text: "The galaxies beyond our grasping metaphors inspire bitter vengeance: the sciences, creative arts and so-called wonder "},
{text: "The pain of wanting bleeds into hatred of what you want"},
{text: "What a strange, strange war the men and women in the city have dressed for! "},
{text: "Desire negates its professor, as its objects expand"},
{text: "Hills and valleys of some odd topography pull on every object"},
{text: "One night, my body was disheveled to a tangle of snakes, and every question of eating or standing or breathing was thrown open"},
{text: "A land without depressions is just a map"},
{text: "Hunger never becomes responsible "},
{text: "With toothsome, handsome faces, corralled celebrities dress the global-industrial-informational billions up as a village "},
{text: "Some questions lead you to a butt plug with an eye that slowly opens and closes"},
{text: "Desire flickers out like the lights, revealing a landscape devoid of intrinsic value, and, hopefully, something else"},
{text: "Some folks say the trees are going out of business"},
{text: "Everyone is a substitute for someone else, back through the service entrance of biography to the beginning of time. Tracing it all back wouldn’t be worth it is the sense I get"},
{text: "Vengeance fills in the details of the final object of desire"},
{text: "If I don’t exist, then why am I only hearing about it now?"},
{text: "The addicts form a circle beside the obelisk, in the shadow of the cannon"},
{text: "A promise hollers to be remembered in the current of druthers, ruby cabernet and amber rye, the wedding ring diluted in a dozen other rings, in ancient Jerusalem, late for a plane"},
{text: "A little oblivion can be salutary"},
{text: "Our bodies are not obscene. But their demands on one another, and on ourselves, often are "},
{text: "Beach nipples stare out—vicious brown anemone mouths"},
{text: "I rise on arms like towers of skulls and thrust into a tunnel of blindness that necessarily narrows"},
{text: "What you would call a failure of love is in fact its survival"},
{text: "Making love in the light of an alarm clock and a baby monitor, a bruise shifts its countenance "},
{text: "A lot of what some folks call truth is just where guesses tend to gutter out"},
{text: "In lucid or blessed moments, what comes from us seems instead to come to us. The wind spins the fan blade. The heat abates "},
{text: "The face is still there. But below, the legs walk a pianola treadmill "},
{text: "The shortcuts through the pun and the notorious number are untrustworthy, but ancient "},
{text: "Every year there were more dinosaurs in the Bible"},
{text: "Music makes fugitives of us—a gull on the wing one day, clinging like barnacle the next "},
{text: "That the tequila shot stays down she marks with a nod: A victory and an assent to all that comes next, even if it’s only another drink"},
{text: "You’ll never impress G-d with your perfection "},
{text: "The bar accurately reflects our status as fallen creatures "},
{text: "The bridge devoured forty city blocks to get so high. Heed the cost, for the cost heeds not you"},
{text: "Strange voices cluster around mundane objects, insisting that we look elsewhere"},
{text: "If you want cheap, comfortable pants, then you must be comfortable with what it took to get them to you so cheaply, and with what you’ll overlook to be comfortable"},
{text: "I order one more glass of whatever will make me feel like a fact in a world of rumor"},
{text: "The final prayer is not for a vision of adamantine completeness to shatter the skulls of porridge-indifferent peers, but for a clue to console my ultimate failure "},
{text: "I am drunk enough to know that I’m not drunk enough"},
{text: "It’s one of those afternoons when you’d burn down the city to buy some time. But where will you spend the time? And don’t say another city"},
{text: "There are immoral obligations in a glass of tap water, and the escalator demands blood. Spandrels of cowardice drown us scoundrels"},
{text: "The expenditure required to clean a place and make it worth cleaning is the stuff of saintly faith and devotion  "},
{text: "Being alive is an untenable position, exacerbated by the insane stakes of selfhood. It’s enough to make a person cautious"},
{text: "He was trapped by time, remorse, desire, disgrace, and some gossamer thread of success. Sound familiar?"},
{text: "Welts and songs notwithstanding, Ultimate Reality takes the opportunities it finds"},
{text: "I’m a cautious, clever fellow. If I were to be honest, what would that even sound like?"},
{text: "Error is the signature "},
{text: "The men gathered to investigate reality and the intentions of its Author, using a billion-dollar sports team. They put some money on the outcome, just to make it interesting "},
{text: "UFO lily pads never quite touch reality and never quite escape"},
{text: "A wine cork and a cigarette filter stained with lipstick crowd the cobbles. Washington Square surges between the misspelled word and the word spilled right"},
{text: "What will you wear on your visit to the parts of your mind over which you have little say?"},
{text: "A billion perfections violently collide to make this thing"},
{text: "I drink because I may yet slur into accuracy"},
{text: "If you can, be careful where you go. Some places keep a corner of you forever"},
{text: "The left eye of G-d bores a hole in the bend of my brow to unearth the moment I yelled at the dog and the addict in the park"},
{text: "The karmic spindle of the American Northeast calls us to spin and be twisted "},
{text: "Go crazy enough for long enough, and you get an ear for what mental health actually sounds like"},
{text: "I wouldn’t trade it for all the grenadine on Fort Hamilton army base"},
{text: "Mothers and fathers tested each other with tales of the comedown room at the Worcester Centrum"},
{text: "Warm air from the gulf—a nice night to read ransom notes"},
{text: "I wouldn’t trade it for all of Orson Welles’ rubber noses"},
{text: "Gentle moments embroil me in a billion-year conspiracy of happiness"},
{text: "One day you will grow strong enough that you can afford to remember what was done to you "},
{text: "The wicker headboard is a veil over all possibilities, like the surface of the sea. Beyond it waits something that justifies concealment"},
{text: "Each granted wish ups the ante"},
{text: "This banter is a scheme of domestication exported to the ghosts and the gods by middle managers of the soul."},
{text: "The mind runs rings around the heart, but can’t leave it behind"},
{text: "Maybe love is just enemies-against-enemies in a heist of darkest night. But who’s to say we don’t fall in love?"},
{text: "Hometown alchemy transmutes what you hate most into the dearest part of your life"},
{text: "The lace of the veil employs every mystery—that we may flit from misconception to misconception until we properly conceive"},
{text: "Butchers and cops are kind to one another  "},
{text: "Pigeons swim the intersection above the jurisdiction of the trash can—testing time, wind, leaves and the wariness on every face. It’s all a test, all a result  "},
{text: "Every face is a holy question poorly phrased"},
{text: "To call life a part of something larger seems like a stalling tactic"},
{text: "Engine and radio on, the passing forms form a metaphor for what they are"},
{text: "Always the glass, for seeing and for safety "},
{text: "Invisibility is one more thread in the mask"},
{text: "Distance insists on a space to sell or seduce ourselves back into"},
{text: "Pain addicts and rubberneckers buzz like moths around the darkest bulbs"},
{text: "Aliens, ghosts and the internet—those final railings of comprehension—seem to be saying something at first, but only say what we think we want to hear"},
{text: "You and I cut a swath through more than just time and space"},
{text: "People have apologized for me. I never counted them as doing me a favor"},
{text: "They caught the fat man licking gizzards in a state of gustatory sin, his epiglottis all untucked"},
{text: "Past and future loop like cursive little bows to and from each of the invisibly small knots that we, in exasperation, call the present "},
{text: "What it’s for is not how it looks is not how it feels "},
{text: "You complain that sympathy corrupts. We, the corrupt, can sympathize"},
{text: "Every new number is one color gone "},
{text: "The avian muscles of a woman’s back and the straining pilasters in an ocean wave are palliatives to interpretation"},
{text: "Illiterate predators effortlessly track me through a wilderness of symbol "},
{text: "Atheist Jehovas and spherical Molochs grow confident that final comprehension is near. The planet closes in like an unhappy home"},
{text: "Sometimes a gentle, interesting thing delivers us from the world"},
{text: "We said we agreed to disagree. But I don’t think we did"},
{text: "The cessation of inquiry, even from honest exhaustion, is the death of a man, the death of the sun. Or it’s the birth of a face"},
{text: "This holiday season, remember: The sociopathic institutions who kick people out of their homes are the only ones who can feed the homeless "},
{text: "This holiday season, remember: The companies who steal you away from your family all year can help you win their affection "},
{text: "This holiday season, remember: With each passing winter, the price of survival gets more convoluted, but never lower"},
{text: "Don’t ever kiss the hologram. But don’t ever let it see you refuse  "},
{text: "The kink in the crooked timber of humanity is a chore slipping perennially down the cosmic to-do list "},
{text: "You could play it straight. It isn’t straight, though, is it?"},
{text: "The wonderful, terrible thing happened to me"},
{text: "Winning isn’t like you think: Each resident of the dungeon is a dull ache of despair for the king"},
{text: "Mushroom-cloud water towers dot the postwar suburbs of Long Island"},
{text: "Oh to be a man as free as the markets are supposed to be!"},
{text: "Oh to be as scary and wonderful as the worst curse word!"},
{text: "My body is collateral I wash and feed, left behind on a promise "},
{text: "So you flee to the traffic, the great middle-distance gaze of the earth—assembled for something else, going somewhere else, the embodiment of else itself"},
{text: "The face of the Holy Mother surfaces from the center of Taylor Ham to announce there’s more to us than our nutritional value or our price, even on special"},
{text: "The inmates started a terrible fire"},
{text: "I get the feeling that I’m up against someone who is wiser than me, but who is not to be trusted"},
{text: "Language is so specific because the bulk of it tends to emanate from the least free people in each generation"},
{text: "In this do-it-yourself country, people mostly do things to themselves"},
{text: "The world doesn’t end, but ebbs into inefficient habits of speech"},
{text: "I have seen love bend the lawyers’ language to hieroglyphics, and I’ve seen time turn a best friend to unreliable rapping on a seance table"},
{text: "How good some songs sound on a cheap plastic radio!"},
{text: "The shoplifter checks his haul of devices, designer water bottles, headphones, wallets, belts, peeling off labels. Over a train ride, everyone becomes an accomplice "},
{text: "Money’s finite. Intelligence’s finite. Nerve’s finite. Charm’s finite. And every night when you go outside you see what’s not"},
{text: "The mental illness sprung from the ruins of Coney Island may yet save us sinners in the hands of an angry network of sinners in an angry network"},
{text: "Better uses for us in the coming year!"},
{text: "Troubles pile up when you try stay inside an institution that can’t manage, or doesn’t remember, to be an institution  "},
{text: "We are still here. We know what miracles feel like, and we don’t care the cost. So, don’t lose faith. We’ve never lost faith in you"},
{text: "Maybe honest men and women aren’t honest because they’re too dumb to lie! Impossible! "},
{text: "If a sin is a debt owed to G-d, and a debt binds two parties, then don’t be too eager for forgiveness or too fastidious in your atonement"},
{text: "I couldn’t stand around anymore with the ten million silent-film stars waiting for their luck to change back"},
{text: "Infinite power is for fools. To be alive is to maneuver amid innumerable limitations. It’s as trivial as a dance. But what a partner!"},
{text: "Where I Find the Time: Every day’s a jigsaw puzzle while someone bangs on the door. Absolute fucking freedom for twenty-two minutes, if you keep it quiet—deal?"},
{text: "Within the tradition of the skeleton, warpaths and peacepaths mark the jowls and the flaring temple arcs from a million squints"},
{text: "They keep a lot of gravity there. It’s mostly imported, and expensive"},
{text: "A lovely tattoo marked out for the tumor the place to make its home"},
{text: "Why not toss one more dictat atop the creaky tower of automaticity with which you greet the minute and the month?"},
{text: "The mind is the itch that believes it’s a scratch "},
{text: "Santa Claus disintegrates into geometric forms, but you never escape his charming gaze"},
{text: "Ashamed to mention the times when we didn’t expect the burly earth to be ripped from under our blue suede shoes, we search out the rally point from the last failed assault "},
{text: "Forever’s not my problem, except for about five minutes a day. It’s a manageable devotion"},
{text: "I narrow down the riddle a little, while carnivorous plants crowd the front yard"},
{text: "It’ll be unbearable around three in the afternoon. And it’ll still be unbearable after a dozen dozen satoris"},
{text: "The screens are muzzles for my eyes. Then visible reality is a muzzle for my eyes. Then I come to a place where my eyes are muzzles "},
{text: "Among the handlers of Pan, there are paperback books that do something even if no one reads them"},
{text: "In the afternoon at the last tangle of wires before the bay, men hose down the not-for-hire oil trucks—forest green and emergency red"},
{text: "Under the expressway spur, my son’s eyes darken to a supernatural blue"},
{text: "The city is symmetrical. If you miss anything on the way in, you can get it on the way out"},
{text: "A time is just a place you can’t return to"},
{text: "Some of these days are hard, and I can’t explain why"},
{text: "The highways are bad, but the distances are worse"},
{text: "The problem was the size of the miracles. I needed a truly miniscule miracle to get the gist of how a miracle might work"},
{text: "The land blisters with seers. The vision is as sensible as the details of a car going 60 MPH past my nose, sensible as the fronds and ribbons of a car wreck "},
{text: "Complaining we have to live, complaining we have to die, who can bear creatures so tiresome as we?"},
{text: "Knowledge was a stage like adolescence, and no less painful "},
{text: "The killing may be done by the sick. But the sickness was inflicted by the healthy "},
{text: "Just think what it’ll be like to die, my little french fry"},
{text: "We can meet by the goiter in the interstate. Don’t be late, unless you’re late"},
{text: "Another time, perpendicular to time, extends from its flat plane"},
{text: "Why is hate still holy?"},
{text: "Tomorrow you’ll think something totally different. But this will still be true "},
{text: "Maybe I spent too much time where lives work and wives lurk "},
{text: "Maybe I knew that I’d walk from this fire, into an incredible darkness "},
{text: "Maybe I wasn’t such a fool as I needed to be"},
{text: "So far, my shabby understanding has done so much less harm than it could have"},
{text: "I am not a cosmos. I can go outside and get a cup of coffee"},
{text: "At the end the day will break. And you’ll see you haven’t made a single mistake"},
{text: "The baby will go on to say something altogether different"},
{text: "One truth doesn’t need to know another truth to be true"},
{text: "There is a return. We know this as surely as the maze confounds us"},
{text: "One question before we start: Do we want to get it more right or more wrong?"},
{text: "If it’s real fear, you may not know until years after it lifts. It will have seemed like sound reasoning in the best of all possible worlds"},
{text: "There's never been a sky so blue as to know no vultures "},
{text: "Reserve the right to be rather unreasonable"},
{text: "One of the great curses laid upon humanity is the abject inability to tell a blessing from a curse"},
{text: "There’s a fire. Everyone crowds the door to matter"},
{text: "Under pressure with no plan—so comes G-d or disaster, in famously unequal measure "},
{text: "Why is absolutely everything still under consideration? I thought it was late"},
{text: "Reserve egress"},
]

// sort according to returned vale of compare func
Aphorisms_array = Aphorisms_array.sort(() => Math.random() - 0.5)
export const randomized_aphorisms_array = Aphorisms_array
