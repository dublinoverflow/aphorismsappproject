// SwipeCards.js
// from https://github.com/meteor-factory/react-native-tinder-swipe-cards
'use strict';

import React, { useEffect, useState } from "react";

//Text,
import { StyleSheet, View, Image, Dimensions, Button, Pressable } from "react-native";
import { Text } from 'react-native-elements';
import SwipeCards from "react-native-swipe-cards-deck";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {randomized_aphorisms_array} from "./Aphorisms.js";
import { Header } from 'react-native-elements';
import { useWindowDimensions } from 'react-native';

import * as Haptics from 'expo-haptics';


//const currentWidth = Dimensions.get('window').width;
//const currentHeight = Dimensions.get('window').height;

/*
Dimensions.addEventListener("change", (e) => {

  console.log(e)

  console.log("Dimensions.get('window').width --> " + Dimensions.get('window').width)
  console.log("Dimensions.get('window').height -->" + Dimensions.get('window').height)

  //currentWidth = Dimensions.get('window').width;
  //currentHeight = Dimensions.get('window').height;
  currentWidth = Dimensions.get('window').width;
  currentHeight = Dimensions.get('window').height;

})
*/




function MyCustomLeftComponent () {
  return (
    <Text>Balls</Text>
  )
}

function MyCustomCenterComponent() {
  return (
    <Text>Nuts</Text>
  )
}




// https://reactnative.dev/docs/pressable
/*
function Card({ data }) {
  return (
      <Pressable onPress={() => alert("this is onPress .....")}>
        <View style={[styles.card]}>
          <Text style={styles.cardText}>{data.text}</Text>
        </View>
      </Pressable>
  );
}
*/


/*
function StatusCard() {
  return (
    <View>
      <Text style={styles.statusCard} h1 >Forget This Good Thing I Just Said</Text>
      <Text style={styles.statusCard} h4 >Aphorisms</Text>
      <Text style={styles.statusCard} h4 >By</Text>
      <Text style={styles.statusCard} h4 >Colin Dodds</Text>
    </View>
  );
}
*/



const App = (props) => {

    const windowWidth = useWindowDimensions().width;
    const windowHeight = useWindowDimensions().height;

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: "black",
        alignItems: "center",
        justifyContent: "center",
        //width: currentWidth,
        //height: currentHeight,
      },
      card: {
        justifyContent: "center",
        alignItems: "center",
        width: windowWidth,
        height: windowHeight,

        padding: 20,

        //borderRadius: 10,
        //backgroundColor: "#f2f3f4",
        backgroundColor: "black",
      },
      cardsText: {
        fontSize: 24,
        justifyContent: "center",
        textAlign: "center",
        color: "purple"
      },
      statusCard: {
        justifyContent: "center",
        textAlign: "center",
        color: "white",
        paddingTop: 15
      },

      headingTest: {
        color: "blue"
      },

      italicsAphorismString: {
        fontStyle: "italic"
      }
    });




    function StatusCard() {
      return (
        <View>
          <Text style={styles.statusCard} h1 >Forget This Good Thing I Just Said</Text>
          <Text style={styles.statusCard} h4 >Aphorisms</Text>
          <Text style={styles.statusCard} h4 >By</Text>
          <Text style={styles.statusCard} h4 >Colin Dodds</Text>
        </View>
      );
    }



    var cardsTextAdjustedViaSettings = {
      color: props.currentTheme[1]
    }
    var cardBackgroundAdjustedViaSettings = {
      backgroundColor: props.currentTheme[0]
    }

    var cardFontsizeAdjustedViaSettings = {
      fontSize: props.currentFontSize
    }


    // new, moved this inside the App

    function Card({ data }) {
      // for some reason, wrapping this callback in setTimeout() makes the can't update component from inside another component error go away
      setTimeout(() => {
        props.currentStateCallback(data.text + "\n\n - An aphorism by Colin Dodds \n Please visit https://forgetthisgoodthing.com for more");
      }, 1);




      if (typeof(data.text) === 'string') {
        var aphorismComponent = data.text;
      } else {
        var aphorismComponent = React.cloneElement(data.text, {textcolor: cardsTextAdjustedViaSettings});
      }



      return (
          <Pressable >
            <View style={[styles.card, cardsTextAdjustedViaSettings, cardBackgroundAdjustedViaSettings]}>
              <Text style={[styles.cardsText, cardsTextAdjustedViaSettings, cardFontsizeAdjustedViaSettings]}>
              {aphorismComponent}
              </Text>
            </View>
          </Pressable>
      );
    }


    // setup a hook
    // https://reactjs.org/docs/hooks-state.html
    const [cards, setCards] = useState();

    // replace with real remote data fetching
    useEffect(() => {
      setTimeout(() => {
        //setCards(aphorisms);
        setCards(randomized_aphorisms_array);
      }, 1);
    }, []);

    function handleYup(card) {
      console.log(`Yup for ${card}`);
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy)
      return true; // return false if you wish to cancel the action
    }
    function handleNope(card) {
      console.log(`Nope for ${card}`);
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy)
      return true;
    }
    function handleMaybe(card) {
      console.log(`Maybe for ${card}`);
      return true;
    }


    // trying to figure out how to reload the cards array,
    // removing:
    // renderNoMoreCards={() => <StatusCard text="No more cards..." />}

    return (

      <View style={styles.container}>
        {cards ? (
          <SwipeCards
            cards={cards}
            renderCard={(cardData) => <Card data={cardData} />}
            loop={true}
            //keyExtractor={(cardData) => String(cardData.text)}
            keyExtractor={(cardData) => cardData.text}
            renderNoMoreCards={() => <StatusCard text="No more cards..." />}
            handleYup={handleYup}
            handleNope={handleNope}
            handleMaybe={handleMaybe}
            hasMaybeAction={true}
            showYup={false}
            showNope={false}
            showMaybe={false}
            smoothTransition={true}

            // If you want a stack of cards instead of one-per-one view, activate stack mode
            // stack={true}
            // stackDepth={3}
          />
        ) : (
          <StatusCard />
        )}
      </View>
    );


}


export default App;
